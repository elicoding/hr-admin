<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offersalary extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offer_salary_breakup';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    
    protected $fillable = ['profile_id','ctc', 'basic', 'basic_td', 'hra','hra_td','spl_allowance','spl_allowance_td','emp_pf','gross_td','gross_td_y','emp_pf_td','gratuity_td','linked_variable_allocate','total_cost','statutory_bonus','attendance_bonus','statutory_bonus_year','attendance_bonus_year','linked_variable_allocate_msg','esic','esic_y'];

    
}
