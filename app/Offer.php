<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Offer extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    
    protected $fillable = ['user_id','first_name', 'last_name', 'email', 'dob','gender','recuiter_desc','source','ssource','mobile','street','city','state','zip','country','sstreet','scity','sstate','szip','scountry','emp_type','offer_number'];

    public function reportdata(){
     $report= DB::table('offers')
    ->select('offers.offer_number as Offer ID',DB::raw('CONCAT(offers.first_name, " ", offers.last_name) AS Name'),DB::raw('DATE_FORMAT(offers.dob, "%d-%m-%Y") as DOB'),'offer_detail.manager as Manager Name','offer_detail.business_unit as BU','offer_detail.designation as Designation','offer_detail.function as Function','offers.email as Personal Email ID',
          'offer_detail.grade_level as Salary Grade',  'offers.mobile as Contact No','offer_detail.shift as Shift Timing','offers.recuiter_desc as HR Name',DB::raw('DATE_FORMAT(offers.created_at, "%d-%m-%Y") as Offer_Date'),DB::raw('DATE_FORMAT(offer_detail.doj, "%d-%m-%Y") as DOJ'),'offer_detail.cab as Cab Required',DB::raw('CONCAT(offers.street, ", ", offers.city,", ", offers.state,", ", offers.country,", ", offers.zip) AS Cab_address'),'offers.gender as Gender','offer_detail.calender as Location')
    ->join('offer_detail', 'offers.id', '=', 'offer_detail.user_id')
    ->join('offer_salary_breakup', 'offers.id', '=', 'offer_salary_breakup.profile_id')
    
    ->get();
      return $report;
    }
 public function reportlist(){
     $report= DB::table('offers')
    ->select('offers.*', 'offer_detail.*')
    ->join('offer_detail', 'offers.id', '=', 'offer_detail.user_id')
    ->join('offer_salary_breakup', 'offers.id', '=', 'offer_salary_breakup.profile_id')
    
    ->get();
      return $report;
    }
}