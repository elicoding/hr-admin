<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Offer;
use App\User;
use App\Offerdetail;
use App\Offersalary;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Item;
use Vsmoraes\Pdf\Pdf;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    
     private $pdf;
      public function __construct(Pdf $pdf)
    {
        $this->pdf = $pdf;
        $this->middleware('auth');
        $this->rec_list  =$rec_list = ['Gurleen Kaur'=>'Gurleen Kaur','Shivali Joshi'=>'Shivali Joshi','Shaili Chauhan'=>'Shaili Chauhan','Reji Kuruvilla'=>'Reji Kuruvilla','Reema Shah'=>'Reema Shah','Nidhi Balli'=>'Nidhi Balli','Kritika  Bawa'=>'Kritika  Bawa','Meghanshi Gupta'=>'Meghanshi Gupta','Priyanka Sharma'=>'Priyanka Sharma','Srishti Wadhawa'=>'Srishti Wadhawa','Seema Nainwal'=>'Seema Nainwal','Anupriya Young'=>'Anupriya Young','Dilip Kumar Chaurasia'=>'Dilip Kumar Chaurasia','Pradeep Simon'=>'Pradeep Simon','Padmini Ieeja'=>'Padmini Ieeja','Subhadra Shinde'=>'Subhadra Shinde','Nancy Lekhi'=>'Nancy Lekhi','Nidhi Sharma'=>'Nidhi Sharma','Veney Kaul'=>'Veney Kaul','Anisha Srivastavah'=>'Anisha Srivastava','Manisha'=>'Manisha','Deepa Sharma'=>'Deepa Sharma'];
         asort($this->rec_list);
       
       $this->gender_list=$gender_list = ['m'=>'Male','f'=>'Female'];
      $this->timing=$timing=["8:00 AM to 5:00 PM"=>"8:00 AM to 5:00 PM",
"9:00 AM to 6:00 PM"=>"9:00 AM to 6:00 PM",
"10:00 AM to 7:00 PM"=>"9:00 AM to 6:00 PM",
"11:00 AM to 8:00 PM"=>"11:00 AM to 8:00 PM",
"12:00 PM to 9:00 PM"=>"12:00 PM to 9:00 PM",
"1:00 PM to 10:00 PM"=>"1:00 PM to 10:00 PM",
"1:30 PM-10:30PM"=>"1:30 PM-10:30PM",
"2:00 PM to 11:00 PM"=>"2:00 PM to 11:00 PM",
"3:00 PM to 12:00 PM"=>"3:00 PM to 12:00 PM",
"4:30 PM to 1:30 AM"=>"4:30 PM to 1:30 AM",
"5:30 PM to 2:30 AM"=>"5:30 PM to 2:30 AM",
"6:30 pm - 3:30 am"=>"6:30 pm - 3:30 am",
"7:00 PM to 4:00 AM"=>"7:00 PM to 4:00 AM",
"7:30 PM to 4:30 AM"=>" 7:30 PM to 4:30 AM",
"8:30 PM to 5:30 PM"=>"8:30 PM to 5:30 PM",
          "8:30 PM- 5:30 AM"=> "8:30 PM- 5:30 AM"];

       $this->employee_type =$employee_type=["sales"=>"sales","nonsales"=>"nonsales"];
       $this->source=$source=['referral'=>'referral','Careers'=>'Careers','vendor'=>'vendor','Campus placement'=>'Campus placement','others'=>'Others','Direct'=>'Direct','Rehire'=>'Rehire','Portal'=>'Portal'];
        
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10000;
       
       // echo $id = Auth::user()->getId();
        $id = \Auth::user()->id;
        $roles= \Auth::user()->roles->first()->name;
        if($roles=='admin'){
        if (!empty($keyword)) {
            $offer = Offer::where('first_name', 'LIKE', "%$keyword%")
                ->orWhere('last_name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('dob', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $offer = Offer::paginate($perPage);
        }
        }else{
           if (!empty($keyword)) {
               if($id=='11'){
                   $id=array('11','12','13');
                   
                   $offer = Offer::whereIn('user_id', $id)->paginate($perPage);
               }else{
            $offer = Offer::where('user_id', $id)->paginate($perPage);
               }
        } else {
             if($id=='11'){
                   $id=array('11','12','13');
                   $offer = Offer::whereIn('user_id', $id)->paginate($perPage);
             }else{
            $offer = Offer::where('user_id', $id)->paginate($perPage);
               }
        }  
        }
        
        return view('offer.index', compact('offer','gender_list','rec_list','id','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $user_id = \Auth::user()->id;
         $gender_list=$this->gender_list;
          $rec_list=$this->rec_list;
          $source=$this->source;
          $employee_type=$this->employee_type;
          
        return view('offer.create',compact('gender_list','rec_list','source','employee_type','user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'first_name' => 'required|string|max:255',
			'last_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:offers',
			'dob' => 'required|before:-18 years',
            'gender' => 'required',
            'recuiter_desc' => 'required',
            'source' => 'required',
            'mobile' => 'required',
             'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'sstreet' => 'required',
            'scity' => 'required',
            'sstate' => 'required',
            'szip' => 'required',
            'scountry' => 'required'
		]);
        $requestData = $request->all();
        
        $offer=Offer::create($requestData);
        $offerId=$offer->id;
        $number ="EGA-00".$offerId;
        $offer = Offer::findOrFail($offerId);
        
        $offer->update(array('offer_number'=>$number));
        return redirect('admin/offer/genrate/'.$offerId)->with('flash_message', 'Offer added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $offer = Offer::findOrFail($id);

        return view('offer.show', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $offer = Offer::findOrFail($id);

         
           $roles= \Auth::user()->roles->first()->name;
        if($roles=='admin'){
            $user_id =$offer->user_id;
        }else{
             $user_id = \Auth::user()->id;
        }
          $gender_list=$this->gender_list;
          $rec_list=$this->rec_list;
          $source=$this->source;
          $employee_type=$this->employee_type;

       // return view('offer.index', compact('offer','gender_list'));
        return view('offer.edit', compact('offer','gender_list','rec_list','source','employee_type','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'dob' => 'required|before:-18 years',
            'gender' => 'required',
            'recuiter_desc' => 'required',
            'source' => 'required',
            'mobile' => 'required',
            
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'sstreet' => 'required',
            'scity' => 'required',
            'sstate' => 'required',
            'szip' => 'required',
            'scountry' => 'required'
        ]);
        $requestData = $request->all();
        
        $offer = Offer::findOrFail($id);
        $offer->update($requestData);

        return redirect('admin/offer/genrate/'.$id)->with('flash_message', 'Offer updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Offer::destroy($id);

        return redirect('admin/offer')->with('flash_message', 'Offer deleted!');
    }
    
    public function genrate($id)
    {
        $stiming=$this->timing;
        $offer = Offer::findOrFail($id);
        $offerdetaildata = Offerdetail::where('user_id', $id)->first();
        $offersalarydata = Offersalary::where('profile_id', $id)->first();
        
      if(!empty($offersalarydata)){
           $offerdetail = Offerdetail::findOrFail($offerdetaildata->id);             
        }
        if(!empty($offerdetaildata)){
           $offersalary = Offersalary::findOrFail($offersalarydata->id);  
          unset($offersalary->profile_id);
        }
    if(!empty($offersalarydata) && !empty($offerdetaildata)){ 
        $result=array_merge($offersalary->toArray( ),$offerdetail->toArray( ));
        $offerdetail= (object)$result;
    }else{
        $offerdetail='';
    }
        
        $company_list =["Eli Research India Private Limited"=>"Eli Research India Private Limited",
            "Eli BPO India Private Limited"=>"Eli BPO India Private Limited",
            "Eli Revenue Cycle Solutions Private Limited"=>"Eli Revenue Cycle Solutions Private Limited",
            "Eli Shared Services Private Limited"=>"Eli Shared Services Private Limited",
            "Eli Health Solutions Private Limited"=>"Eli Health Solutions Private Limited",
            "GBIG Business Solutions Private Limited"=>"GBIG Business Solutions Private Limited"
           
            ];
            //$location_list=['Chennai - IN'=>'Chennai - IN',	'Chennai - US'=>'Chennai - US',	'Faridabad-Mahalaxmi-Canada'=>'Faridabad-Mahalaxmi-Canada',	'Faridabad-Mahalaxmi-IN'=>'Faridabad-Mahalaxmi-IN',	'Faridabad-Mahalaxmi-US'=>'Faridabad-Mahalaxmi-US',	'Faridabad-SSR-Canada'=>'Faridabad-SSR-Canada',	'Faridabad-SSR-IN'=>'Faridabad-SSR-IN',	'Faridabad-SSR-US'=>'Faridabad-SSR-US',	'Gurgaon - US'=>'Gurgaon - US',	'Hyderabad - IN'=>'Hyderabad - IN',	'Hyderabad - US'=>'Hyderabad - US',	'Indore'=>'Indore',	'Noida-IN'=>'Noida-IN',	'Noida-US'=>'Noida-US'];
            $location_list=['Chennai'=>'Chennai','Faridabad'=>'Faridabad','Hyderabad'=>'Hyderabad','Indore'=>'Indore','Noida'=>'Noida'];
            $designation_list=['Account Manager'=>'Account Manager',	'Analyst'=>'Analyst',	'Assistant Financial Controller'=>'Assistant Financial Controller',	'Assistant Manager'=>'Assistant Manager',	'Assistant Team Lead'=>'Assistant Team Lead',	'Assistant Team Leader'=>'Assistant Team Leader',	'Assistant Technical Manager'=>'Assistant Technical Manager',	'Assistant Vice president'=>'Assistant Vice president',	'Associate'=>'Associate',	'Associate Analyst'=>'Associate Analyst',	'Associate Business Analyst'=>'Associate Business Analyst',	'Associate Consultant'=>'Associate Consultant',	'Associate Director'=>'Associate Director',	'Asterisks/ViCI'=>'Asterisks/ViCI',	'Audit Coordinator'=>'Audit Coordinator',	'Business Analyst'=>'Business Analyst',	'Business Development Executive'=>'Business Development Executive',	'Business Development Manager'=>'Business Development Manager',	'CEU Vendor Analyst'=>'CEU Vendor Analyst',	'CEU Verification Representative'=>'CEU Verification Representative',	'Chief Operating Officer'=>'Chief Operating Officer',	'Chief Technical Officer'=>'Chief Technical Officer',	'Coder'=>'Coder',	'Coding Coach'=>'Coding Coach',	'Collection Executive'=>'Collection Executive',	'Consultant'=>'Consultant',	'Content Marketing Expert'=>'Content Marketing Expert',	'Coordinator'=>'Coordinator',	'Corporate Counsel'=>'Corporate Counsel',	'Data Architect'=>'Data Architect',	'Data Entry Operator'=>'Data Entry Operator',	'Deputy Manager'=>'Deputy Manager',	'Deputy Technical Manager'=>'Deputy Technical Manager',	'Desktop Support Engineer'=>'Desktop Support Engineer',	'Developer'=>'Developer',	'Director'=>'Director',	'Electrician'=>'Electrician',	'End User Support'=>'End User Support',	'End User Support/L2'=>'End User Support/L2',	'Engineer'=>'Engineer',	'Event & Data Assistant'=>'Event & Data Assistant',	'Event Manager'=>'Event Manager',	'Exam Coordinator'=>'Exam Coordinator',	'Executive'=>'Executive',	'Finance Controller'=>'Finance Controller',	'Financial Controller'=>'Financial Controller',	'FRONT END DEVELOPER'=>'FRONT END DEVELOPER',	'General Manager'=>'General Manager',	'Head'=>'Head',	'Helpdesk'=>'Helpdesk',	'Instuctional Design Analyst'=>'Instuctional Design Analyst',	'Junior Executive'=>'Junior Executive',	'Junior Software Engineer'=>'Junior Software Engineer',	'Junior support engineer'=>'Junior support engineer',	'Junior Tech Associate'=>'Junior Tech Associate',	'Layout Executive'=>'Layout Executive',	'Lead'=>'Lead',	'Lead CEU Vendor Analyst'=>'Lead CEU Vendor Analyst',	'Lead Finance Analyst'=>'Lead Finance Analyst',	'Lead Medical Auditor'=>'Lead Medical Auditor',	'Linux admin'=>'Linux admin',	'LOCAL CHAPTER CORDINATOR'=>'LOCAL CHAPTER CORDINATOR',	'Management Trainee'=>'Management Trainee',	'Manager'=>'Manager',	'Manager Corporate Strategy'=>'Manager Corporate Strategy',	'Medical Auditor'=>'Medical Auditor',	'Module Lead'=>'Module Lead',	'Network Support'=>'Network Support',	'Officer'=>'Officer',	'Offshore Program Manager'=>'Offshore Program Manager',	'Online Marketing Head'=>'Online Marketing Head',	'Operation Manager'=>'Operation Manager',	'Operations Head'=>'Operations Head',	'Payroll Executive'=>'Payroll Executive',	'President'=>'President',	'President & CEO'=>'President & CEO',	'Procurement Supervisor'=>'Procurement Supervisor',	'Product Analyst'=>'Product Analyst',	'Product Configurator'=>'Product Configurator',	'Program Manager'=>'Program Manager',	'Project Lead'=>'Project Lead',	'Project Manager'=>'Project Manager',	'Publishing Manager'=>'Publishing Manager',	'Quality Analyst'=>'Quality Analyst',	'Release Manager'=>'Release Manager',	'Research & Relationship Executive'=>'Research & Relationship Executive',	'Research & Relationship Lead'=>'Research & Relationship Lead',	'Sales Head'=>'Sales Head',	'Salesforce Developer'=>'Salesforce Developer',	'Senior Account Manager'=>'Senior Account Manager',	'Senior Analyst'=>'Senior Analyst',	'Senior Associate'=>'Senior Associate',	'Senior Business Analyst'=>'Senior Business Analyst',	'Senior Business Development Executive'=>'Senior Business Development Executive',	'Senior Business Development Manager'=>'Senior Business Development Manager',	'Senior Coder'=>'Senior Coder',	'Senior Console Operator'=>'Senior Console Operator',	'Senior Consultant'=>'Senior Consultant',	'Senior Customer Service Associate'=>'Senior Customer Service Associate',	'Senior Database Administrator'=>'Senior Database Administrator',	'Senior Designer'=>'Senior Designer',	'Senior Desktop Publisher'=>'Senior Desktop Publisher',	'Senior Developer'=>'Senior Developer',	'Senior Dot Net Developer'=>'Senior Dot Net Developer',	'Senior Engineer'=>'Senior Engineer',	'Senior Executive'=>'Senior Executive',	'Senior front end developer'=>'Senior front end developer',	'Senior Frontend Designer'=>'Senior Frontend Designer',	'Senior Frontend Developer'=>'Senior Frontend Developer',	'Senior Graphic Designer'=>'Senior Graphic Designer',	'Senior Linux Administrator'=>'Senior Linux Administrator',	'Senior Manager'=>'Senior Manager',	'Senior Network Architect'=>'Senior Network Architect',	'Senior Process Associate'=>'Senior Process Associate',	'Senior Project Manager'=>'Senior Project Manager',	'Senior QA Engineer'=>'Senior QA Engineer',	'Senior Quality Analyst'=>'Senior Quality Analyst',	'Senior Sales Officer'=>'Senior Sales Officer',	'Senior Software Engineer'=>'Senior Software Engineer',	'Senior Specialist'=>'Senior Specialist',	'Senior System Administrator'=>'Senior System Administrator',	'Senior System Support Engineer'=>'Senior System Support Engineer',	'Senior Tech Associate'=>'Senior Tech Associate',	'Senior Technical Analyst'=>'Senior Technical Analyst',	'Senior Technical Executive'=>'Senior Technical Executive',	'Senior Technical Specialist'=>'Senior Technical Specialist',	'SENIOR TECHNOLOGY ANALYST'=>'SENIOR TECHNOLOGY ANALYST',	'Senior UI/UX Designer'=>'Senior UI/UX Designer',	'SEO Analyst'=>'SEO Analyst',	'SEO Specialist'=>'SEO Specialist',	'Server Admin'=>'Server Admin',	'Social Media Analyst'=>'Social Media Analyst',	'Software Engineer'=>'Software Engineer',	'Specialist'=>'Specialist',	'Subject Matter Expert'=>'Subject Matter Expert',	'System Administrator'=>'System Administrator',	'Team Lead'=>'Team Lead',	'Team Lead - Accounting'=>'Team Lead - Accounting',	'Team Leader'=>'Team Leader',	'Tech Associate'=>'Tech Associate',	'Tech Lead'=>'Tech Lead',	'Tech Lead Dotnet'=>'Tech Lead Dotnet',	'Technical Account Manager'=>'Technical Account Manager',	'Technical Architect'=>'Technical Architect',	'Technical Head'=>'Technical Head',	'Technical Lead'=>'Technical Lead',	'Technical Manager'=>'Technical Manager',	'Technical Specialist'=>'Technical Specialist',	'Technical Support Engineer'=>'Technical Support Engineer',	'Testing Engineer'=>'Testing Engineer',	'Trainee'=>'Trainee',	'UI Technical Lead'=>'UI Technical Lead',	'Vertical Head'=>'Vertical Head',	'Vice president'=>'Vice president',	'Windows System Administrator'=>'Windows System Administrator',
'Lead Software Engineer'=>'Lead Software Engineer','Graphic Designer'=>'Graphic Designer','QC'=>'QC',"Sr. QA II"=>"Sr. QA II", "Sr. Developer II"=>"Sr. Developer II","Jr. Data Analyst"=>"Jr. Data Analyst",
                "QA I"=>"QA I",
"Developer I"=>"Developer I",
"QA II"=>"QA II",
"Developer II"=>"Developer II",
"QA III"=>"QA III",
"Developer III"=>"Developer III",
"QA IV"=>"QA IV",
"Developer IV"=>"Developer IV",
"Senior QA I"=>"Senior QA I",
"Senior Developer I"=>"Senior Developer I",
"Senior QA II"=>"Senior QA II",
"Senior Developer II"=>"Senior Developer II",
"Senior QA III"=>"Senior QA III",
"Senior Developer III"=>"Senior Developer III",
"Principal QA I"=>"Principal QA I",
"Principal Developer I"=>"Principal Developer I",
"Principal QA II"=>"Principal QA II",
"Principal Developer II"=>"Principal Developer II",
"Principal QA III"=>"Principal QA III",
"Principal Developer III"=>"Principal Developer III",
"Module Lead"=>"Module Lead",
"Project Lead"=>"Project Lead",
"QA Project Lead"=>"QA Project Lead",
"Business Analyst"=>"Business Analyst",
"Technical Lead"=>"Technical Lead",
"Team Lead"=>"Team Lead",
"QA Lead"=>"QA Lead",
"Project Manager"=>"Project Manager",
"Product Owner"=>"Product Owner",
"Associate Architect"=>"Associate Architect",
"Senior Project Manager"=>"Senior Project Manager",
"Senior Product Owner"=>"Senior Product Owner",
"Architect"=>"Architect",
"Senior Architect"=>"Senior Architect",
"Release Manager"=>"Release Manager",
                "Analyst"=>"Analyst" ,
				 "AR Analyst"=>"AR Analyst" 
				
                ];
            $function_list=['.net'=>'.net',	'Account Management'=>'Account Management','Due Diligence'=>'Due Diligence','Accounting'=>'Accounting',	'Accounts'=>'Accounts',	'Accounts Payable'=>'Accounts Payable',	'Accounts Receivable'=>'Accounts Receivable',	'Accounts Receivables'=>'Accounts Receivables',	'ADMIN'=>'ADMIN',	'Administration'=>'Administration',	'Administration & Projects'=>'Administration & Projects',	'Android Development'=>'Android Development',	'Application Support'=>'Application Support',	'Apps'=>'Apps',	'ASP.NET'=>'ASP.NET',	'Audit'=>'Audit',	'Backend Operations'=>'Backend Operations',	'Backoffice Administration'=>'Backoffice Administration',	'Billing'=>'Billing',	'Business Analytics'=>'Business Analytics',	'Business Development'=>'Business Development',	'Business HR'=>'Business HR',	'Business Operations'=>'Business Operations',	'CEU Vendor Services'=>'CEU Vendor Services',	'CEU/Membership Operations'=>'CEU/Membership Operations',	'Claims'=>'Claims',	'Client Service'=>'Client Service',	'Coding'=>'Coding',	'Coding Content Writer'=>'Coding Content Writer',	'Collections'=>'Collections',	'Collections Invoicing'=>'Collections Invoicing',	'Compliance'=>'Compliance',	'Content Factory'=>'Content Factory',	'Content Writing'=>'Content Writing',	'Credit analysis'=>'Credit analysis',	'Customer Service'=>'Customer Service',	'Customer Support'=>'Customer Support',	'Data Analysis'=>'Data Analysis',	'Data Processing'=>'Data Processing',	'Database Administration'=>'Database Administration',	'Deal Processor'=>'Deal Processor',	'Delphi'=>'Delphi',	'Design'=>'Design',	'Designing'=>'Designing',	'Desktop Support'=>'Desktop Support',	'Development'=>'Development',	'Dialer'=>'Dialer',	'Digital Marketing'=>'Digital Marketing',	'Dot Net Developer'=>'Dot Net Developer',	'Dot Net/Cloud lead'=>'Dot Net/Cloud lead',	'Editor'=>'Editor',	'E-Learning'=>'E-Learning',	'Email Marketing'=>'Email Marketing',	'Enterprise Sales'=>'Enterprise Sales',	'Events'=>'Events',	'Exams'=>'Exams',	'Expense Analysis'=>'Expense Analysis',	'Facilities'=>'Facilities',	'Finance'=>'Finance',	'Finance and Accounts'=>'Finance and Accounts',	'FP&A'=>'FP&A',	'Front Office'=>'Front Office',	'Fulfilment'=>'Fulfilment',	'Graphic Designing'=>'Graphic Designing',	'Healthcare'=>'Healthcare',	'HR Operations'=>'HR Operations',	'Human Resources'=>'Human Resources',	'Human Resources & Organization Developme'=>'Human Resources & Organization Developme',	'Information Security'=>'Information Security',	'Information Technology'=>'Information Technology',	'Inside Sales'=>'Inside Sales',	'Insurance'=>'Insurance',	'IOS Development'=>'IOS Development',	'IT'=>'IT',	'IT Asset'=>'IT Asset',	'IT Desktop Support'=>'IT Desktop Support',	'IT Helpdesk'=>'IT Helpdesk',	'IT Procurement'=>'IT Procurement',	'Key Account Management'=>'Key Account Management',	'Layout'=>'Layout',	'Lead Generation'=>'Lead Generation',	'Learning and Development'=>'Learning and Development',	'Legal'=>'Legal',	'Linux'=>'Linux',	'Mainframe'=>'Mainframe',	'Market Research'=>'Market Research',	'Marketing'=>'Marketing',	'Marketing Automation'=>'Marketing Automation',	'Marketing Operations'=>'Marketing Operations',	'Marketing Research'=>'Marketing Research',	'Medical Auditing'=>'Medical Auditing',	'Medical Billing'=>'Medical Billing',	'Medical Coding'=>'Medical Coding',	'Medical Transcription'=>'Medical Transcription',	'Mergers & Acquisitions'=>'Mergers & Acquisitions',	'MIS'=>'MIS',	'Mobile development'=>'Mobile development',	'MSBI'=>'MSBI',	'Network & Security'=>'Network & Security',	'Network Administration'=>'Network Administration',	'Online Marketing'=>'Online Marketing',	'Operations'=>'Operations',	'Organization Development'=>'Organization Development',	'Partner Support'=>'Partner Support',	'Payroll'=>'Payroll',	'PHP Development'=>'PHP Development',	'Post Sales Training'=>'Post Sales Training',	'Practice Management'=>'Practice Management',	'Prepress'=>'Prepress',	'Print Production'=>'Print Production',	'Print Production/Subscription'=>'Print Production/Subscription',	'Procurement'=>'Procurement',	'Product'=>'Product',	'Product Management'=>'Product Management',	'Project Management'=>'Project Management',	'Publishing'=>'Publishing',	'Quality'=>'Quality',	'Quality and Compliance'=>'Quality and Compliance',	'Quality Assurance'=>'Quality Assurance',	'Research'=>'Research',	'Sales'=>'Sales',	'SEO'=>'SEO',	'Service Delivery'=>'Service Delivery',	'SFDC'=>'SFDC',	'SG&A'=>'SG&A',	'Software Development'=>'Software Development',	'Software Implementation'=>'Software Implementation',	'Software Testing'=>'Software Testing',	'Spanish'=>'Spanish',	'Speaker Relations'=>'Speaker Relations',	'SQL'=>'SQL',	'SQL DBA'=>'SQL DBA',	'Statutory Reporting'=>'Statutory Reporting',	'Subscriptions'=>'Subscriptions',	'System Administration'=>'System Administration',	'Talent Acquisition'=>'Talent Acquisition',	'Technical Support'=>'Technical Support',	'Technology'=>'Technology',	'Technology and Software'=>'Technology and Software',	'Testing'=>'Testing',	'Trainee'=>'Trainee',	'Training'=>'Training',	'Transfer Agent'=>'Transfer Agent',	'Transport'=>'Transport',	'Underwriting and Debt'=>'Underwriting and Debt',	'Web Designing'=>'Web Designing',	'Web Development'=>'Web Development',	'Web Strategist'=>'Web Strategist',	'Web/Programming'=>'Web/Programming',	'Windows Administration'=>'Windows Administration',	'Wintel'=>'Wintel',
              'Underwriter'=>'Underwriter' ];
            $business_unit=['TCI'=>'TCI','A R Allegiance'=>'A R Allegiance',	'AAPC'=>'AAPC',	'Alpine Capital'=>'Alpine Capital',	'Arrevio'=>'Arrevio',	'BCC'=>'BCC',	'Entrust Global Group '=>'Entrust Global Group',	'Canta Health'=>'Canta Health',	'CBS'=>'CBS',	'CBV'=>'CBV',	'Certitrek'=>'Certitrek',	'Clanwilliam Health'=>'Clanwilliam Health','CSI'=>'CSI','ECL Group'=>'ECL Group',	'Eli Corp'=>'Eli Corp',	'Eli Health'=>'Eli Health',	'Eli Leadership Institute'=>'Eli Leadership Institute',	'ELI Recruitment'=>'ELI Recruitment',	'EMR International'=>'EMR International',	'Engaged media'=>'Engaged media',	'Fleet Assist'=>'Fleet Assist',	'FTGU'=>'FTGU',	'GBC'=>'GBC',	'GCC Inc'=>'GCC Inc',	'HCCS'=>'HCCS',	'HCS'=>'HCS',	'Healthicity'=>'Healthicity',	'INSURANCE'=>'INSURANCE',	'Integrity'=>'Integrity',	'IOPW'=>'IOPW',	'Key Medical'=>'Key Medical',	'M & A'=>'M & A',	'Management Plus'=>'Management Plus',	'MBS'=>'MBS',	'MDO'=>'MDO',	'MEDFLOW'=>'MEDFLOW',	'Mortgage'=>'Mortgage',	'My Vision Express(MVE)'=>'My Vision Express(MVE)',	'NRS'=>'NRS',	'PBO'=>'PBO',	'Penn Medical'=>'Penn Medical',	'PMMS'=>'PMMS',	'Pro Edtech'=>'Pro Edtech',	'RCM'=>'RCM',	'Shared- Finance'=>'Shared- Finance',	'Shared-Admin'=>'Shared-Admin',	'ShopperLocal'=>'ShopperLocal',	'TCI Sales'=>'TCI Sales',	'Techradix'=>'Techradix',	'Van Ru'=>'Van Ru',	'Yaras Group'=>'Yaras Group','Fortrex'=>'Fortrex','GTIC'=>'GTIC','Future Source'=>'Future Source', "MBW"=>"MBW","OMPC"=>"OMPC","NPC"=>"NPC"];
           
            $role=['Data Entry Operator '=>'Data Entry Operator ','NA '=>'NA ',	'Trainee '=>'Trainee '];
            $grade_level=['E1-B'=>'E1-B',	'E1-C'=>'E1-C',	'E2-A'=>'E2-A',	'E2-B'=>'E2-B',	'E2-C'=>'E2-C',	'E3-A'=>'E3-A',	'E3-B'=>'E3-B',	'E3-C'=>'E3-C',	'E4-A'=>'E4-A',	'E4-B'=>'E4-B',	'E4-C'=>'E4-C',	'E5-A'=>'E5-A',	'E5-B'=>'E5-B',	'E5-C'=>'E5-C',
];
            $template_list=['Offer letterELI'=>'Offer letterELI','Offer letterELI ESI'=>' ESI Offer letterELI'];
            $cab=['yes'=>'yes','no'=>'no'];
           






            $location_cal=['Faridabad-SSR-IN'=>'Faridabad-SSR-IN','Faridabad-SSR-US'=>'Faridabad-SSR-US','Faridabad-ML-IN'=>'Faridabad-ML-IN','Faridabad-ML-US'=>'Faridabad-ML-US','Faridabad-ML-CN'=>'Faridabad-ML-CN','Chennai-IN'=>'Chennai-IN','Chennai-US'=>'Chennai-US','Hyderabad-IN'=>'Hyderabad-IN','Hyderabad-US'=>'Hyderabad-US','Noida-IN'=>'Noida-IN','Noida-US'=>'Noida-US','Indore'=>'Indore','Mumbai-IN'=>'Mumbai-IN','Mumbai-US'=>'Mumbai-US','Faridabad-Canada'=>'Faridabad-Canada'];
             return view('offer.genrate', compact('stiming','offerdetail','offer','company_list','location_list','function_list','designation_list','grade_list','business_unit','employee_type','role','grade_level','template_list','cab','location_cal'));
    }

     public function saveoffer(Request $request)
    {
		//after:yesterday
        $this->validate($request, [
			'company' => 'required',
			'business_unit' => 'required',
			'location' => 'required',
			
                        'function' => 'required',
                        'grade_level' => 'required',
                        'designation' => 'required',
                        'template' => 'required',
                         'doj' => 'required',   
                         'cab' => 'required',
                        'manager' => 'required',
                     'shift' => 'required',
		]);
        $detailArray=['company'=>$request->input('company'),
            'business_unit'=>$request->input('business_unit'),
            'location'=>$request->input('location'),
           
            'function'=>$request->input('function'),
            'grade_level'=>$request->input('grade_level'),
            'designation'=>$request->input('designation'),
             'template'=>$request->input('template'),
            'doj'=>$request->input('doj'),
            'user_id'=>$request->input('user_id'),
            'cab'=>$request->input('cab') ,
            'manager'=>$request->input('manager') ,
            'shift'=>$request->input('shift'),
            'calender'=>$request->input('calender')
                
                ];
         $linked_variable_allocate=$request->input('linked_variable_allocate');
        if($linked_variable_allocate>0 ){
            $linked_variable_allocate=$request->input('linked_variable_allocate');
        }else{
          $linked_variable_allocate=0;  
        }
        $salArray=['profile_id'=>$request->input('user_id'),
            'ctc'=>$request->input('ctc'),
            'basic'=>$request->input('basic'),
            'basic_td'=>$request->input('basic_td'),
            
            'hra'=>$request->input('hra'),
            'hra_td'=>$request->input('hra_td'),
           
            'spl_allowance'=>$request->input('spl_allowance'),
            'spl_allowance_td'=>$request->input('spl_allowance_td'),
            'emp_pf'=>$request->input('emp_pf'),
            'emp_pf_td'=>$request->input('emp_pf_td'),
            'gross_td'=>$request->input('gross_td'),
            'gross_td_y'=>$request->input('gross_td_y'),
            'gratuity_td'=>$request->input('gratuity_td'),
            'linked_variable_allocate'=>$linked_variable_allocate,
            'linked_variable_allocate_msg'=>$request->input('linked_variable_allocate_msg'),
            'total_cost'=>$request->input('total_cost'),
            'statutory_bonus'=>$request->input('statutory_bonus'),
            'attendance_bonus'=>$request->input('attendance_bonus'),
             'statutory_bonus_year'=>$request->input('statutory_bonus_year'),
            'attendance_bonus_year'=>$request->input('attendance_bonus_year'),
            'esic'=>$request->input('esic'),
            'esic_y'=>$request->input('esic_y'),
            ];
       
        //$requestData = $request->all();
         $user_id = $request->input('user_id');
        $offer = Offer::findOrFail($user_id);
        $offerdetaildata = Offerdetail::where('user_id', $user_id)->first();
        $salarydetaildata = Offersalary::where('profile_id', $user_id)->first();
     
         
        if(!empty($offerdetaildata)){
           $Offerdetail = Offerdetail::findOrFail($offerdetaildata->id);
           $Offerdetail->update($detailArray);
           }else{
            Offerdetail::create($detailArray);  
        }
        if(!empty($salarydetaildata)){
           $Offersalary = Offersalary::findOrFail($salarydetaildata->id);
           $sid=$salarydetaildata->id;
           $Offersalary->update($salArray);
           }else{
             
            $offerSalary=Offersalary::create($salArray);  
            $sid=$offerSalary->id;
        }
        
        $Offersalary = Offersalary::findOrFail($sid);
         
                $items['date'] = date('M d, Y',strtotime($request->input('doj')));
		 $items['first_name'] = ucfirst($offer->first_name);
		 $items['last_name'] = $offer->last_name;//	street
		 $items['address'] =explode(',',$offer->street);
		 $items['city'] = $offer->city;
		 $items['state'] = $offer->state;
		 $items['zip'] = $offer->zip;
		 $items['company'] = $request->input('company');
		 $items['position'] = $request->input('designation')."-".$request->input('function');
		 $items['ctc'] = $request->input('ctc');
		 $items['csign_date'] =date('M d, Y', strtotime(' +1 day'));
                 $items['sign_date'] = date('M d, Y');
		 $items['location'] = $request->input('location');//"Faridabad";
                 $pdf_name=ucfirst($offer->first_name)."_".$offer->last_name."_".$request->input('doj');
		 $html =  view('offer/print_offer',compact('items','Offersalary'));
                 
            return   $this->pdf ->filename($pdf_name)->load($html, 'A4') ->show();

       //return redirect('admin/offer')->with('flash_message', 'Offer Detail added!');
    }
    public function genrateappointment($id)
    {
        $offer = Offer::findOrFail($id);
        $offerdetaildata = Offerdetail::where('user_id', $id)->first();
        $Offersalary = Offersalary::where('profile_id', $id)->first();
        $items['date'] = date('M d, Y',strtotime($offerdetaildata->doj));
		 $items['first_name'] = ucfirst($offer->first_name);
		 $items['last_name'] = $offer->last_name;//	street
		 $items['address'] =explode(',',$offer->street);
		 $items['city'] = $offer->city;
		 $items['state'] = $offer->state;
		 $items['zip'] = $offer->zip;
		 $items['company'] = $offerdetaildata->company;
		 $items['position'] = $offerdetaildata->designation."-".$offerdetaildata->function;
		 $items['ctc'] = $Offersalary->ctc;
                 $items['emp_type'] = $offer->emp_type;
                 
		 $items['csign_date'] =date('M d, Y', strtotime($offerdetaildata->created_at));
                 $items['sign_date'] = date('M d, Y');
		 $items['location'] = $offerdetaildata->location;//"Faridabad";
                 $pdf_name=ucfirst($offer->first_name)."_".$offer->last_name."_".$offerdetaildata->doj;
		 $html =  view('offer/print_appointment',compact('items','Offersalary'));
                // echo $html;
                // die;
                return   $this->pdf ->filename($pdf_name)->load($html, 'A4') ->show();
    }
    
    public static function my($args){
		// do your stuff or return something.
		 $user = User::findOrFail($args);
                 echo $user->name;
                
              
                
	}
}
