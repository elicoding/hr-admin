<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
         // $this->middleware(['auth', 'has_auth_permission']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function index() {
	
       
	$id = Auth::User()->id;       
        $user = User::findOrFail($id);        
        return view('admin.profile.edit', compact('user')); 
    }
	
    public function update(Request $request, $id) {		
		
        $this->validate($request, ['name' => 'required', 'email' => 'required']);

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);

       return redirect('admin')->with('success', 'Profile successfully updated.');
    }
	
	
       public function editprofilepassword($id) {
        $user = User::findOrFail($id);
       
        return view('profile.password', compact('user'));
    }

	 public function profilepassword(Request $request) {
		 
		 $this->validate($request, [           
            'password' => 'required|min:6|confirmed'
        ]);  
		$id=$request->user_id;		
		$user = User::findOrFail($id);  		
        $input['_token'] = $request->_token;	
		$input['user_id'] = $request->user_id;	
		$input['password'] = bcrypt($request->password);	
		$input['user_password'] = $request->password;	
		$input['password_confirmation'] = $request->password_confirmation;			
        $user->fill($input)->save();	
        return redirect()->route('admin');
    }
	

}
