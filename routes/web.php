<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin', 'Admin\AdminController@index');
Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('admin', 'Admin\AdminController@index');
Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
Route::resource('admin/offer', 'Admin\OfferController');
Route::get('admin/offer/genrate/{id}', 'Admin\OfferController@genrate');
Route::get('admin/offer/save-offer', 'Admin\OfferController@saveoffer');
Route::post('admin/offer/save-offer', 'Admin\OfferController@saveoffer');
Route::get('admin/offer/genrateappointment/{id}', 'Admin\OfferController@genrateappointment');
Route::resource('admin/profile', 'Admin\ProfileController');
Route::resource('admin/report', 'Admin\ReportController');
Route::get('admin/import-export-view', 'Admin\ReportController@importExportView')->name('import.export.view');
Route::post('admin/import-file', 'Admin\ReportController@importFile')->name('import.file');
Route::get('admin/export-file/{type}', 'Admin\ReportController@exportFile')->name('export.file');