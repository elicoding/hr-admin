<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
<style>
    html, body {                background-color: #e8e6e7;                color: #636b6f;                font-family: 'Raleway', sans-serif;                font-weight: 100;                height: 100vh;                margin: 0;            }
    .full-height {                height: 100vh;            }
    .flex-center {                align-items: center;                display: flex;                justify-content: center;            }            
    .position-ref {                position: relative;            }
    .top-right {             font-size: 0;     text-align: center;           position: relative;    top: 35px;}
    .content {                text-align: center;            }
    .title {                font-size: 84px;            }
    .links > a {                color: #fff;                padding: 0 25px;                font-size: 12px;                font-weight: 600;                letter-spacing: .1rem;                text-decoration: none;                text-transform: uppercase;      line-height: 40px;    display: inline-block;    border-radius: 3px;      background: #555; margin-top: 10px;          }
    .links > a:hover {    background: #0b2a42;}

    .m-b-md {                margin-bottom: 30px;            }
    .logo-img{ display: inline-block; border-right: 1px solid #fff;     margin-right: 30px;    }
    .logo-img img{border-right: 1px solid #a1a1a1;     padding: 60px 0; padding-right: 30px;     vertical-align: middle;}
    .title{ display: inline-block; }
    .title h2{ font-size: 4.5rem; color: #f89939; text-shadow: 0 1px 0 #111; margin-bottom: 0;     margin-top: 0;position: relative;    top: 40px;}
   
</style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
       

            <div class="container-fluid">
                <div class="logo-img">
                    <img src="../img/egaLogo.png">
                </div>
                <div class="title m-b-md">
                    <h2>HR Portal</h2>

                 @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/admin') }}">Admin</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                      
                    @endauth
                </div>
            @endif
                </div>

       
            </div>
        </div>
    </body>
</html>
