@extends('layouts.backend')

@section('content')
  

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Offer {{ $offer->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/offer') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/offer/' . $offer->id . '/edit') }}" title="Edit Offer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['offer', $offer->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Offer',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $offer->id }}</td>
                                    </tr>
                                    <tr><th> First Name </th><td> {{ $offer->first_name }} </td></tr><tr><th> Last Name </th><td> {{ $offer->last_name }} </td></tr><tr><th> Email </th><td> {{ $offer->email }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
     
@endsection
