@extends('layouts.backend')
@section('content')
<?php use App\Http\Controllers\Admin\OfferController;?>
 <div class="col-lg-12">
                    <h3 class="page-header">Offer Managment <a href="{{ url('/admin/offer/create') }}" class="btn btn-success btn-sm pull-right" title="Add New Offer">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a></h3>
                </div>
   <div class="col-lg-12">
   
                    <div class="panel panel-default">
                        <div class="panel-heading">           
                           Offers Listing
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Offer Id</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                         <th>Recruiter name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @foreach($offer as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->offer_number }}</td>
                                        <td>{{ ucfirst($item->first_name) }}</td>
                                        <td>{{ ucfirst($item->last_name) }}</td>
                                        <td>{{ $item->email }}</td>
                                         <td>{{OfferController::my($item->user_id)}}</td>
                                        <td>
                                            
                                            <a href="{{ url('/admin/offer/' . $item->id . '/edit') }}" title="Edit Offer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            @if($roles=='admin')
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/offer', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Offer',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
											
                                             <a href="{{ url('/admin/offer/genrateappointment/' . $item->id) }}" title="Edit Offer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Appointment Letter</button></a> 
					 @endif
                                        </td>
                                    </tr>
                                @endforeach
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

          
   @section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{ asset('css/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<script type="text/javascript">
 $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
 </script>
@stop
@endsection
 