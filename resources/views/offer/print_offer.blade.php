<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Offer Letter</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
    body{font-size: 11px; color: #000; text-align: justify;}
    </style>
  </head>
  <body>
    <div class="container">
		<div class="row">

<div class="col-sm-12">
   
    <p>
        <img src="img/EGA_Logo.png" style=" max-width: 150px;"/>
    </p>
<p>Date:<b>{{$items['sign_date']}},</b></p>
<p>
<b>
{{$items['first_name']}} {{$items['last_name']}}<br/>
@foreach($items['address'] as $address){{ $address }},<br/>@endforeach
{{$items['city']}}<br/>
{{$items['state']}}, {{$items['zip']}}</b><br/><br/>
Dear <b>{{$items['first_name']}},</b><br/>
</p>

Congratulations!<br/>

We are pleased to confirm that you have been selected to work for <b>Eli Research India Pvt. Ltd. d.b.a Enterprise Growth Accelerator a.k.a EGA </b>(“Company”). We are delighted to make you the following job offer. <br/><br/>

The position we are offering you is that of  <b>{{$items['position']}}</b> at an annual cost to Company of  <b>INR. {{$Offersalary->total_cost}}/-.</b> The break-up of the annual cost to Company is provided in enclosed annexure.<br/><br/>

We would like you to commence your work from  <b>{{$items['date']}}.</b> If this date is not acceptable, please contact the undersigned immediately. Please report to Human Resource Department for documentation and orientation. You will be initially located at <b>{{$items['location']}};</b> however, the Company shall have the right, at its sole discretion, to transfer at any time, your services to any of its affiliates/associate companies or to post you at any other location in India as per the business need. <br/><br/>

Please sign this letter and return it on or before  <b>{{$items['csign_date']}}</b> to indicate your acceptance of this offer, otherwise this offer shall stand cancelled. <br/><br/>

At <b>Eli Research India Pvt. Ltd. d.b.a Enterprise Growth Accelerator a.k.a EGA</b>, we have the best and finest team of professionals in the business who are experts in their own field. We bring on the best talent across the globe and empower them to drive strategy their way! We build long term relations with our people, partners and our customers. We value your abilities and believe you will find our work environment to be challenging and fulfilling. We are confident that you will be able to make a significant contribution to the success of the Company and look forward to working with you.  <br/><br/>

Sincerely,<br/>
Authorized Signatory<br/><br/>
<hr>
I would like to thank you for offering me the position of <b>{{$items['position']}}</b> with <b>Eli Research India Pvt. Ltd. d.b.a Enterprise Growth Accelerator a.k.a EGA</b> After considering your proposal I am very pleased to be able to accept it. I look forward to starting work on <b>{{$items['date']}}</b> and meeting my new work colleagues.<br/><br/><br/>
<div class="row">
  <div class="col-md-9" style="width:70%; display:inline-block;">____________________________<br/><b>{{$items['first_name']}} {{$items['last_name']}}</b></div>
  <div class="col-md-3" style="width:30%;  display:inline-block;">_________________________<br/>Date</div>
</div>
</div>
</div>

<br><br>
<div class="text-center"><b>Eli Research India Pvt. Ltd. d.b.a Enterprise Growth Accelerator a.k.a EGA</b><br/>
    <br/>
      <br/>
</div>

<div><br><br>
<b><i>This is a computer generated document thus no signature is required from Eli.</i></b>
</div>
       <div class="row">
<div class="col-sm-12">   
<p>
  <img src="img/EGA_Logo.png" style=" max-width: 150px;"/>
</p>
</div>
          </div>
        <div class="row">
            <div class="col-sm-12">
                <h4 class="text-center">SALARY ANNEXURE</h4>
                <hr>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <p><b></b></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8" style="width:66.66%; display: inline-block">
                <table>
                    <tbody>
                        <tr>
                            <td><b>Name</b></td>
                            <td> &nbsp;&nbsp;{{$items['first_name']}} {{$items['last_name']}}</td>
                        </tr>
                        <tr>
                            <td><b>Designation</b></td>
                            <td>&nbsp;&nbsp;{{$items['position']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-4" style="width:33.33%; display: inline-block">
                 <table>
                    <tbody>
                        <tr>
                            <td>DOJ</td>
                            <td>&nbsp;&nbsp;{{$items['date']}}</td>
                        </tr>
                        <tr>
                            <td><b>Location</b></td>
                            <td>&nbsp;&nbsp;{{$items['location']}}</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12"> <br>
                <table class="table table-bordered table-condensed table-striped">
                    <thead>
                        <tr>
                           
                            <th>Components</th>
                            <th>Monthly</th>
                            <th>Annual</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            
                            <td>Monthly Basic</td>
                            <td>{{ $Offersalary->basic}}</td>
                            <td>{{ $Offersalary->basic_td}}</td>
                            <td></td>
                        </tr>
                         <tr>
                            
                            <td>HRA</td>
                            <td>{{ $Offersalary->hra}}</td>
                            <td>{{ $Offersalary->hra_td}}</td>
                            <td></td>
                        </tr>
                       
                        <tr>
                            <td>Special Allowance</td>
                           
                            <td>{{ $Offersalary->spl_allowance}}</td>
                            <td>{{ $Offersalary->spl_allowance_td}}</td>
                            <td></td>
                        </tr>
                         <tr>
                           
                            <td>Contribution to Provident Fund</td>
                            <td>{{ $Offersalary->emp_pf}}</td>
                            <td>{{ $Offersalary->emp_pf_td}}</td>
                            <td></td>
                        </tr>
                       
                         @if($Offersalary->statutory_bonus>0)
                        <tr>
                           
                            <td>Statutory Bonus</td>
                            <td>{{ $Offersalary->statutory_bonus}}</td>
                            <td>{{ $Offersalary->statutory_bonus_year }}</td>
                            <td></td>
                        </tr>
                        @endif
                         @if($Offersalary->attendance_bonus>0)
                        <tr>
                           
                            <td>Attendance Allowance</td>
                           <td>{{ $Offersalary->attendance_bonus}}</td>
                            <td>{{ $Offersalary->attendance_bonus_year  }}</td>
                            <td></td>
                        </tr>
                       @endif
                         <tr>
                          
                             <td><b>Gross (A)</b></td>
                           <td>{{ $Offersalary->gross_td}}</td>
                            <td>{{ $Offersalary->gross_td_y}}</td>
                            <td></td>
                        </tr>
                   
                  
                        <tr>
                         
                             <td><b>Gratuity(B)</b></td>
                            <td></td>
                            <td>{{ $Offersalary->gratuity_td}}</td>
                            <td></td>
                        </tr>
                             @if($Offersalary->linked_variable_allocate>0)
                         <tr>
                          
                             <td><b>Performance Linked Variable Pay (C)</b></td>
                           <td></td>
                            <td>{{ $Offersalary->linked_variable_allocate}}</td>
                            <td>{{ $Offersalary->linked_variable_allocate_msg}}</td>
                        </tr>
                         @endif
                              @if($Offersalary->esic>0)
                         <tr>
                          
                             <td><b>Employer Contribution to ESI (C)</b></td>
                           
                            <td>{{ $Offersalary->esic}}</td>
                            <td>{{ $Offersalary->esic_y}}</td>
                             <td></td>
                        </tr>
                         @endif
                         <tr>
                            @if($Offersalary->linked_variable_allocate>0 || $Offersalary->esic>0)
                            <td>Cost To Company (A+B+C)</td>
                           @else
                            <td>Cost To Company (A+B)</td>
							@endif
                            <td></td>
                            <td>{{ $Offersalary->total_cost}}</td>
							<td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
<div class="row">
    <p><b>Notes</b></p>
</div>
<div class="row">
    <div class="col-md-12">
        <ul>
            <li>1500/1000 per month Cab deduction will be made if employee avails cab facility- applicable only for night shift employees</li>
            <li>Gross salary includes contribution of employers to Employee Provident Fund & Employee Pension Scheme</li>
            <li>Any tax liabilities arising out of the remuneration will be deducted as per the Income Tax rules</li>
            <li>Group Personal Accident Insurance coverage- as per company policy</li>
            <li>Statutory Bonus (if applicable)-This is adjustable with any Bonus, Incentive paid during the year.</li>
            <li>Medical Insurance (Not Eligible For ESIC Employee)-Rs. 2 lacs (Single), 4 lacs (Married) & 5 lacs (Married with up to 2 kids)</li>
            <li>If applicable, Rs. 2,000/- Attendance Allowance is paid based on Eli Rules as amended by Eli from time to time however there will be a Rs.1000/- deduction for every instance of absenteeism from Attendance Allowance</li>
            <li>For Chennai, Hyderabad and Indore employees, this includes Employee's contribution to Professional Tax as per slab.</li>
            <li>In addition to the above, Paid Time Off (PTO) and Holidays are available as per company's norms.<br/>
                Details of your compensation plan are confidential, please raise any queries with the undersigned only.<br/>
                “Incentives will be paid over and above the salary basis the achievement of decided sales targets.”
            </li>
 
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p><b>For Eli Research India Private Limited</b></p>
    </div>
</div>
<div class="row">
  <div class="col-md-12" style="width:70%; display:inline-block;"></div>
  
</div>
<div class="row">
  <div class="col-md-9" style="width:70%; display:inline-block;">____________________________<br/><b>Authorized Signatory</b><br/></div>
  <div class="col-md-3" style="width:30%;  display:inline-block;">_________________________<br/>{{$items['first_name']}} {{$items['last_name']}}</div>
</div>

<div><br><br>
<b><i>This is a computer generated document thus no signature is required from Eli.</i></b>
</div>
</div>
   
      
</body>
</html>

