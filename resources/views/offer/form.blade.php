<style>  label.required:after {
    color: #cc0000;
    content: "*";
    font-weight: bold;
    margin-left: 5px;
}
</style>
    <div class="panel panel-default">
                        <div class="panel-heading">
                           Primary Inforamtion
                        </div>
                        <div class="panel-body">
 <div class="col-md-6">
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label required']) !!}  
    <div class="col-md-6">
        {!! Form::text('first_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('last_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
    {!! Form::label('dob', 'Date of Birth', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('dob', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::select('gender', $gender_list, null, ['class' => 'form-control'])!!}
        
        
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>

</div>
 <div class="col-md-6">
    <div class="form-group {{ $errors->has('resume_desc') ? 'has-error' : ''}}">
    {!! Form::label('recuiter_desc', 'Recuiter Name', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
         {!! Form::select('recuiter_desc', $rec_list, null, ['class' => 'form-control'])!!}
         
        
        {!! $errors->first('recuiter_desc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 
  <div class="form-group {{ $errors->has('emp_type') ? 'has-error' : ''}}">
    {!! Form::label('emp_type', 'Employee Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
           {!! Form::select('emp_type',$employee_type, null, ['class' => 'form-control'])!!}
        {{ Form::hidden('user_id', $user_id, array('id' => 'user_id')) }}
        {!! $errors->first('emp_type', '<p class="help-block">:message</p>') !!}
    </div>
    </div>
    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
         {!! Form::text('mobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
    </div>
<div class="form-group {{ $errors->has('source') ? 'has-error' : ''}}">
    {!! Form::label('source', 'Source', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::select('source', $source, null, ['class' => 'form-control'])!!}
       
    {!! $errors->first('recuiter_desc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
  <div class="form-group {{ $errors->has('ssource') ? 'has-error' : ''}}">
    {!! Form::label('', '', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
         {!! Form::text('ssource', null, ('required' == 'required') ? ['class' => 'form-control'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('ssource', '<p class="help-block">:message</p>') !!}
    </div>       
       

</div>

 </div>
</div>
</div>
    <div class="panel panel-default">
<div class="panel-heading">
                            Current Address
                        </div>
                        <div class="panel-body">
<div class="col-md-6">
<div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
    {!! Form::label('street', 'Street', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('street', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', 'State', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('state', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('country', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
</div>
<div class="col-md-6">
    <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('city', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    {!! Form::label('zip', 'Zip', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('zip', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
    </div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading"> Permanent Address <label class="checkbox-inline">
                                                <input type="checkbox" id="same_as_billing">Same As Current Address
                                            </label></div>

                                            
                                            
<div class="panel-body">
<div class="col-md-6">
    <div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
    {!! Form::label('sstreet', 'Street', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('sstreet', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', 'State', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('sstate', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('scountry', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
</div>
<div class="col-md-6">
<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('scity', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('scity', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    {!! Form::label('zip', 'Zip', ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
          {!! Form::text('szip', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        
        {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
    </div>
</div>
</div>
</div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-default']) !!}
        <button type="reset" class="btn btn-default">Reset Button</button>
    </div>
</div>

