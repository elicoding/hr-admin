<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Appointment Letter</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
    body{font-size: 10px; color: #000; text-align: justify;}
    li{list-style:none;margin-bottom: 10px; }
    .btm-footer{width:100%; position: absolute; bottom:0px; margin-top: 35px;}
    .page-wise{padding-bottom: 0; height: 980px;    position: relative; }
    </style>
  </head>
  <body>
    <div class="container">
        <div class="page-wise">
		<div class="row">

<div class="col-sm-12">
    <br/>
<img src="img/EGA_Logo.png" style=" max-width:150px;"/>
    
<p>Date:<b>{{$items['sign_date']}},</b></p>
<p>
<b>
{{$items['first_name']}} {{$items['last_name']}},<br/>
@foreach($items['address'] as $address){{ $address }},<br/>@endforeach
{{$items['city']}}<br/>
{{$items['state']}}, {{$items['zip']}}</b><br/><br/>

<b>Subject : Letter of Appointment</b><br/>

<br/>Dear <b>{{$items['first_name']}},</b>
</p>
This letter of appointment is being issued pursuant to your acceptance and receipt of the 
offer letter dated {{ $items['csign_date'] }}. <br/><br/>
We now hereby confirm your appointment as <b>{{$items['position']}}</b> for <b>Eli Research India Pvt. Ltd. (d.b.a Enterprise Growth Accelerator a.k.a EGA)</b> 
(hereinafter to be referred as “Company”) commencing from {{$items['date']}} on such remuneration as already agreed by you as per the said offer letter. 
We wish you all the very best and look forward to developing a mutually beneficial association. <br/><br/>
Please note that your employment with Company shall be governed by the terms and conditions as mentioned
below and policies, rules and regulations of the Company as specifically mentioned herein by way of reference and 
in force or as amended or altered from time to time (duly notified to you). <br/><br/>
This letter of appointment shall supersede all prior negotiations, agreements and understandings with respect to your 
employment hereof and/or your employment with the Company, unless specifically incorporated herein by way of reference.<br/><br/>
<br/>
<p>
<b>1. REMUNERATION :</b><br>
a. Your salary shall be as per the annexure signed by you with the said Offer Letter (reannexed
herewith) and shall be paid as per the Payroll Policy of the Company;<br/><br/>
b. Your salary shall be subject to legal and statutory deductions including on account of tax, provident
fund and any other legal or statutory deduction that may be introduced during the term of your
employment with the Company.
</p>
<br/>
<p>
<b>
2. PLACE OF POSTING AND TRANSFER:</b><br>
You will presently be stationed at {{$items['location']}};. However, the Company shall have the right, at its sole
discretion, to transfer at any time, your services to any of its branch offices in India or worldwide as per its
or its affiliates’/subsidiaries’/group companies’ business requirement.
</p>
<br/>
<p>
<b>
3. PROBATION :</b><br>
@if($items['emp_type']=='nonsales')
<br>You will be on probation for an initial period of Three (03) months from the date of your joining the Compnay and this period of Three (03) months can be extended by the Company at its sole discretion subject to your performance. During the Probation Period your services are liable to be dispensed with at any time by either side by giving Fifteen (15) days’ notice or upon payment of salary in lieu thereof without assigning any reason whatsoever. However, in the event you resign from your services during your probation period, the Company as per its business requirements may opt to either relieve you earlier (without any liability of paying of any notice period that you were to serve) or in the event you are in middle of an assignment, extend your notice period to allow you to complete all operative parts of the assignment. 
@else
  <br>  You will be on probation for an initial period of Six (06) months from the date of your joining the Compnay and this period of Six (06) months can be extended by the Company at its sole discretion subject to your performance. During the Probation Period your services are liable to be dispensed with at any time by either side by giving Thirty (30) days’ notice or upon payment of salary in lieu thereof without assigning any reason whatsoever. However, in the event you resign from your services during your probation period, the Company as per its business requirements may opt to either relieve you earlier (without any liability of paying of any notice period that you were to serve) or in the event you are in middle of an assignment, extend your notice period to allow you to complete all operative parts of the assignment. 
@endif
</p>
 <p>
<b>4. REFERENCE CHECK AND DATA PROTECTION :</b><br/>
  
  a.By placing your signature on this letter of appointment you are confirming that your 
employment with the Company is based on the academic, professional, personal details and other 
information submitted by you (hereinafter referred to as “Personal Data”) with the Compnay and 
    is subject to a reference check by the Company before or during the course of your employment. 
    You further agree to notify in writing immediately to the Company any change affecting the 
    Personal Data provided by you to the Company in order to hold and maintain them accurate and updated.<br/><br/>
b.By signing on this letter of appointment you hereby authorize the Company to verify, validate,
    transfer, store, process all the Personal Data submitted by you or otherwise obtained by the Company 
    and to share such Personal Data with other companies, whether in India or abroad, to ensure compliance 
    with this letter of appointment and its legal obligations such as income tax and social security 
    withholdings, management, and other necessary human resources, management, and employment
    related matters<br/><br/>
c.If at any time during the your employment with the Company, it emerges that the Personal Data
    furnished by you are false/incorrect or if any material or relevant information has been suppressed
    or concealed, your employment shall be considered ineffective and irregular and may be 
    liable to be terminated by the Company, at Company’s sole discretion.<br/>
    
</p>
</div>

                    </div>
              </div>

                    <div class="page-wise">
        <div class="row">
            <div class="col-sm-12">
                 <img src="img/EGA_Logo.png" style=" max-width:150px;"/>       
<p>
<b>5. YOUR RESPONSIBILITIES:</b><br/>
<ul>
<li>a. Unless prevented by sickness, injury or other incapacity (subject to proof of records) or as
otherwise agreed by your reporting manager (in writing), you will devote your time exclusively to the
business and affairs of the Company.<br/>
<li>b. During the term of your employment, you shall not engage in any business or enterprise or work
directly or indirectly for any other employer or be a director of any other company, gratuitously or for
profit, without the previous specific written consent of the Company. In addition, you should not
have any interest, financial or otherwise, directly or indirectly, in any firm, company or body with
whom the Company has, or contemplates having, business relations without disclosing the fact in
writing to the Company immediately when it comes to your knowledge that such business relations
are being contemplated or made.</li>
<li>c. During your employment, you may be called upon to work at any of the Company’s establishments,
subsidiary or associate of the Company and / or to undertake tours or other assignments in
connection with the company’s activities.</li>
<li>d. You should not enter into any commitments or dealings on behalf of the Company for which you
have no express authority nor shall you become a party to any alteration of any principle or policy
of the Company or exceed the authority or discretion vested in you without the previous sanction of
the Company’s management.</li>
<li>e. During your employment with the Company and/or thereafter, you shall not do any act, deed or
thing which may damage or harm the reputation of the Company or any of its affiliates or directors
or stakeholders.</li>

<li>f. In addition to the duties and obligations specified in this letter of appointment, you shall abide by all
the rules, regulations, policies and procedures framed by the Company from time to time and
brought to your notice duly.</li>
</ul>

</p>
<p>
    <b> 6. REPRESENTATION AND WARRANTIES :</b><br/>
You represent and warrant that:<br/>
<ul><li>a. There are no restrictions or prohibitions that may prevent you from fully and properly undertaking any of your obligations herein. You acknowledge that if any such restriction or prohibition comes into existence, is enforced, and will prohibit or prevent you from fully and properly undertaking any of your employment obligations pursuant to this letter of appointment, you will inform the Company within seven (07) days and make good faith efforts to eliminate such restriction or prohibition. If such restriction or prohibition is not removed within thirty (30) days, then the Company may terminate your employment as per the terms of this letter of appointment.</li>
<li>b. You will not make or cause to be made any payment, loan or gift of money or anything of value, directly or indirectly, to or for the benefit of any official, employee or commercial agent of any government, governmental authority, public agency, public enterprise or public international organization or to any political party or candidate thereof, where, such payment, loan or gift of money or anything of value would constitute a bribe or a facilitation payment or is intended to influence a decision in favor of the Company.</li>
<li>c. You will not post any denigrating comments or statements or remarks against the Company (and its employees) on social websites/blogs and/or misrepresent the Company in whatsoever manner.</li>
</ul>
</p>
<p>
    <b>7. INTELLECTUAL PROPERTY RIGHTS:</b>
<ul>
<li>a. All Intellectual Property which you may make or conceive, either solely or jointly with others, during the period of your employment with the Company, shall be deemed to be the sole property of the Company. Further, it shall be your duty to promptly reduce to writing and to disclose to the Company all such Intellectual Property which you may make or conceive.</li>
<li>b. You hereby expressly waive all of your rights and interests in and to the Intellectual Property and shall at the request of the Company execute such documents as may be required for assignment of the Intellectual Property.</li>
<li>c. The rights and obligations under this Clause in respect of Intellectual Property made or discovered by you during your employment shall continue in full force and effect after the termination of your employment and shall be binding upon you and your representatives in perpetuity.
<li>For the purpose of this Clause “Intellectual Property” shall mean any and all now known or hereafter known tangible and intangible rights vested with the Company, as the case may be, and shall include  (a) licenses rights, (b) trade and service marks, (c) trade or business names, domain names, (d) rights in designs, copyrights, for the full term of such rights and including any extension to or renewal of the terms of such rights and including registrations and applications for registration of any of these and rights to apply for the same and all rights and forms of protection of a similar nature or having equivalent or similar effect to any of these anywhere in the world, (d) other intellectual or industrial property rights (of each kind and nature throughout the universe and however designated) (including logos, “rental” rights and rights to remuneration), whether arising by operation of law, contract, license, or otherwise</li>
</ul>
</p>
 <p>
    <b>8. NON-COMPETE, CONFIDENTIALITY AND NON-SOLICITATION :</b><br/>
    <ul>
        <li>In reference to the Non-Competition, Confidentiality and Non-solicitation Agreement that you had signed at the time of joining the Company, you agree that the said Non-Competition, Confidentiality and Non-solicitation Agreement constitutes to be part and parcel of your employment terms and thus shall be read in conjunction with this letter of appointment.</li>
</ul>
</p>
 </div>
        </div>
  

                    </div>




<div class="page-wise">

        <div class="row">
            <div class="col-sm-12">
                   <img src="img/EGA_Logo.png" style=" max-width:150px;"/>  
 
<p> <b> 9. INDEMNITY AND LIABILITY :</b><br/>
Upon your failure to comply with the terms and conditions of this letter of appointment and/or Non-Competition, Confidentiality and Non-solicitation Agreement, the Company shall be entitled to initiate civil and criminal proceedings against you including but not limited to Section 72A of the Information Technology Act, Section 403 i.e. Dishonest misappropriation of property and Section 405 i.e. Criminal breach of trust under the Indian Penal Code, 1860. At all times you should indemnify and save harmless the Company from all and every loss, injury, costs, damages, etc., which has been or shall or may at any times or time hereafter, the Company may suffer as a result of any act or omission on your part or in the event of any breach of the terms and conditions of your employment.</br>
<br/>You should protect, indemnify and save harmless the Company from and against any and all damages, claims, suits, actions, judgments, costs and expenses whatsoever (including reasonable legal/attorney fees) arising out of breach of the terms of this letter of appointment or in connection with act or omission on your part during the course of your employment with the Company.
</p>              
<p>
    <b> 10. SEVERANCE :</b><br/>
    <ul>
<li>a.	After confirmation of your services with the Company, the Company may terminate your employment by serving a written notice of 60 (Sixty) days or payment of salary in lieu thereof. Likewise, after being confirmed, you may resign from your services by serving 60 (Sixty) days’ notice period or salary in lieu thereof. However, in the event, you resign from your services, the Company as per its business requirements may: (i) opt to relieve you earlier (without any liability of paying of any notice period that you were to serve) or; (ii) extend your notice period to allow you to complete all operative parts of business assignment(s) in the event you are in middle of an assignment.</li>
<li>b. 	Notwithstanding anything contrary contained herein, your employment shall be terminated forthwith without (any notice obligation) in the event you are found to have committed gross misconduct. By the expression "gross misconduct" shall be meant any of the following acts and omissions on your part:</li>
<ul> <li>i.	engaging in any trade or business outside the scope of your duties except with the written permission of the Company;  </li>
<li>ii.	unauthorized disclosure of information regarding the affairs of the Company or any of its customers or any other person connected with the business of the Company which is confidential or the disclosure of which is likely to be prejudicial to the interests of the Company;  </li>
<li>iii.	found to be in a drunken or intoxicated or disorderly state or are found behaving indecently or any conducting in similar behavior on the premises of the Company; </li>
<li>iv.	willful damage or attempt to cause damage to the property of the Company or any of its client/vendors; </li>
<li>v.	willful insubordination or disobedience of any lawful and reasonable order of the management or of a superior;  </li>
<li>vi.	slowing down in performance of work and/or underrated performance even after being given performance feedback by the reporting manager;  </li>
<li>vii.	gambling or betting on the premises of the Company;  </li>
<li>viii.	doing any act prejudicial to the interest of the Company or gross negligence or negligence involving or likely to involve the Company in serious loss; </li>
<li>ix.	giving or taking a bribe or illegal gratification from a client or a prospective/current employee of the Company or a vendor of the Company; </li>
<li>x.	abetment or instigation of any of the acts or omissions abovementioned knowingly making a false statement in any document pertaining to or in connection with your employment in the Company; </li>
<li>xi.	remaining absent without intimation continuously for a period exceeding 03 (Three) days; </li>
<li>xii.	misbehavior towards Company’s customers or services providers; </li>
<li>xiii.	conviction by a criminal court of law; </li>
<li>xiv.	indulging in any act of sexual harassment at workplace. Wherein sexual harassment shall include such unwelcome sexually determined behavior (whether directly or otherwise) as physical contact and advances, or demand or request for sexual favors, or sexually colored remarks, or showing pornography, or any other unwelcome physical/verbal/non-verbal conduct of a sexual nature;  </li>
<li>xv.	if you are found to be involved or have committed in any anti-national or illegal or unlawful activity; </li>
<li>xvi.	material breach of the terms of the Non-Competition, Confidentiality and Non-solicitation Agreement that you had signed at the time joining. </li>
<li>However, depending on the enormity of the circumstance, in the event of any of the above listed gross misconducts being found to be committed on your part, apart from the right to terminate your services forthwith, the Company at its discretion may also choose to bring you down to lower scale of pay; and/or your increment/s and/or promotions may also be stopped for a period; and/or any and all allowances given to you may also be withdrawn; and/or you may also be fined
</li>
</ul>
    </li>
    </ul>
</p>
          </div>
        </div>
   
    

</div>
<div class="page-wise">
  
        <div class="row">
<div class="col-sm-12">   
 <img src="img/EGA_Logo.png" style=" max-width:150px;"/>
<p>
<b>11. EFFECT OF SEVERANCE :</b><br/>
<ul>
<li>a.In the event of termination of your services by the Company, the Company shall only be obligated to provide you with payment of accrued remuneration and leave encashment as per the leave policy of the Company in lieu of unavailed accrued privilege leave standing to your credit, due and payable up to the last date of your employment with the Company. It is hereby clarified that no further payments in respect of incentives of whatsoever nature would be made to you in the event your services are terminated by the Company.</li>
<li>b.Upon resigning from your services or termination of your employment (for whatever reason and howsoever arising) you:</li>
<li><ul> <li>i. shall not take away, conceal or destroy but shall immediately handover to the Company all
documents and information (which expression shall include, without limitation, notes,
software, correspondence, drawings, plans, designs and any other material upon which data
or information is recorded or stored) relating to the business or affairs of the Company or any
affiliate or any of their customers, shareholders, directors, employees, officers, clients,
suppliers and agents (and you shall not be entitled to retain any copies or reproductions of
any such documents) together with any other property belonging to the Company which may
then be in your possession or under your control;</li>
<li>ii. shall not at any time thereafter make any untrue or misleading oral or written statement
concerning the business and affairs of the Company or any affiliate nor represent yourself or
permit yourself to be held out as being in any way connected with or interested in the
business of the Company or any affiliate (except as a former employee for the purpose of
communicating with prospective employers or complying with any applicable statutory
requirements)</li>
<li>iii. should immediately return to the Company such items or materials (pertaining to or
concerning the Company or the business of the Company) under your control or in your
possession or otherwise dispose of or destroy as per the written directions of the Company;</li>
<li>iv.immediately return all items of equipment held on loan or hire from the Company.</li>
    </ul>
</li>
</ul>
<p><b>12. COMMUNICATIONS:</b><br/></br>
All notices given pursuant to this Agreement shall be in writing and shall be deemed to be served as
follows:<br/></br>
<br/>a. In the case of any notice delivered by hand, when so delivered;:
<br/>b. If sent by speed post or registered post, on the third clear day after the date of posting;</br>
<br/>c. If sent by e- mail, 24 hours after the mail is sent by either party to the other party at the respective addresses available with the parties as per the available records.<br/>
</p>
<p><b>13. GOVERNING, JURISDICTION AND DISPUTE RESOLUTION :</b><br/>
<br/>a.	This letter of appointment shall be governed in all respect by the laws of India and subject to the arbitration clause as contained herein below the courts at Delhi shall have exclusive jurisdiction.<br/>
<br/>b. If any dispute, difference or claim including but not limited to the matter of damages, if any, (collectively referred to as "Dispute") arises between 
you and Company about the validity, interpretation, implementation or alleged breach of any provision of this appointment letter, or anything connected or 
related to or incidental to this appointment letter, then the Dispute shall be submitted to arbitration before the sole arbitrator to be appointed by the 
Company. The arbitration shall be conducted in accordance with the provisions of the Arbitration and Conciliation Act 1996, as amended from time to time. 
Arbitration shall be held at Delhi, India. The arbitration proceedings shall be conducted and the award shall be rendered in the English language. 
The arbitrator will be required to make the award within two (2) months of entering upon the reference unless the time is extended by the Company. 
The award rendered by the arbitrator shall be final, conclusive and binding and shall be subject to enforcement in any court of competent jurisdiction.
The cost of arbitration, including attorney’s fees and expenses of the arbitrator shall be borne by the parties equally unless otherwise directed by the arbitrator in the final award.<br/>
</p>
<p>
14. ENFORCEMENT:</b><br/>
<br/>Without limitation to the foregoing, you consent and agree that if you will violate any of the provisions of this Agreement with respect to secrecy, non-compete, confidential information or Intellectual Property, the Company or any of its affiliates shall be entitled to an injunction to be issued by any court of competent jurisdiction, restraining you from committing or continuing any violations of this letter of appointment, to  claim damages and all other appropriate relief as ordered by the court
</p><p>
<b>15. SEVERABILITY :</b><br/>
<br/>In the event that any provision of this letter of appointment, shall be construed as being invalid or unenforceable, such invalidity or unenforceability shall not affect any of the other provisions in this letter of appointment which can be given effect without the invalid or unenforceable provision.
</p>
</div>
          </div>
		    

</div>
<div class="page-wise">


        <div class="row">
<div class="col-sm-12">   
<img src="img/EGA_Logo.png" style=" max-width:150px;"/>
    <p>
<b>16. SURVIVAL :</b><br/>
<br/>The termination or expiration of your employment shall in no event terminate or prejudice (a) any right or obligations arising out of or accruing under this letter of appointment or any other agreement that you have signed pursuant to your employment with the Company, attributable to events or circumstances occurring prior to such termination or expiration; and (b) the provisions of Intellectual Property, Confidential Information and Non-compete Obligation, Notice, Governing Law and Dispute Resolution and Liability and Indemnity shall remain in full force and effect.
</p><p>
<b>17. ENTIRETY :</b><br/>
<br/>The termination or expiration of your employment shall in no event terminate or prejudice (a) any right or obligations arising out of or accruing under this letter of appointment or any other agreement that you have signed pursuant to your employment with the Company, attributable to events or circumstances occurring prior to such termination or expiration; and (b) the provisions of Intellectual Property, Confidential Information and Non-compete Obligation, Notice, Governing Law and Dispute Resolution and Liability and Indemnity shall remain in full force and effect.
</p>
<p>
               We would request you to acknowledge receipt of this letter and signify your acceptance of the terms of
employment and expected date of joining by signing and returning the attached duplicate copy.</p>
<b>Eli Research India Pvt. Ltd. d.b.a. Enterprise Growth Accelerator a.k.a EGA</b><br><br><br><br>
____________________________<br>
Authorized Signatory<br><br><br><br>


<p>I,{{$items['first_name']}} {{$items['last_name']}} ,have carefully read understood the aforesaid conditions of my appointment and employment with Eli Research India Pvt. Ltd. and voluntarily accept the same without any coercion or undue influences.  I understand that my employment is contingent upon execution of the Non-competition, Confidentiality and Non-solicitation Agreement and compliance with Company’s policies copy of which have been duly shared with you and you have been given complete opportunity to read them.
</p><br><br>
Full Signature: ________________________________<br><br>
Full Name: ________________________________<br><br>
PAN/AADHAR No.: ________________________________<br><br>
<p>
By signing below further I confirm having read and understood the below listed policies:</br><br><br>
<br>1. Anti –Sexual Harassment Policy<br>
<br>2. Code of Conduct Policy<br>
<br>3. Information Security Policy<br>
<br>4. Leave Policy<br>
<br>5. Working Hour Policy<br>
<br>And I confirm having been given complete access to go through these and other binding policies.<br><br>


<br>Full Signature: ________________________________<br>
<br>Full Name: ________________________________<br>
<br>PAN/AADHAR No.: ________________________________<br><br><br>

</div>
          </div>
		   
                    

</div>

                    <div class="page-wise">

  
        <div class="row">
            <div class="col-sm-12">
                 <img src="img/EGA_Logo.png" style=" max-width:150px;"/>  
                <h4 class="text-center">SALARY ANNEXURE</h4>
                <hr>
            </div>
        </div>
        
     
        <div class="row">
            <div class="col-sm-8" style="width:66.66%; display: inline-block">
                <table>
                    <tbody>
                        <tr>
                            <td><b>Name</b></td>
                            <td> &nbsp;&nbsp;{{$items['first_name']}} {{$items['last_name']}}</td>
                        </tr>
                        <tr>
                            <td><b>Designation</b></td>
                            <td>&nbsp;&nbsp;{{$items['position']}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-4" style="width:33.33%; display: inline-block">
                 <table>
                    <tbody>
                        <tr>
                            <td><b>D.O.J</b></td>
                            <td>&nbsp;&nbsp;{{$items['date']}}</td>
                        </tr>
                        <tr>
                            <td><b>Location</b></td>
                            <td>&nbsp;&nbsp;{{$items['location']}}</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered table-condensed table-striped">
                    <thead>
                        <tr>
                           
                            <th>Components</th>
                            <th>Monthly</th>
                            <th>Annual</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            
                            <td>Monthly Components</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            
                            <td>Monthly Basic</td>
                            <td>{{ $Offersalary->basic}}</td>
                            <td>{{ $Offersalary->basic_td}}</td>
                            <td></td>
                        </tr>
                         <tr>
                            
                            <td>HRA</td>
                            <td>{{ $Offersalary->hra}}</td>
                            <td>{{ $Offersalary->hra_td}}</td>
                            <td></td>
                        </tr>
                       
                        <tr>
                            <td>Special Allowance</td>
                           
                            <td>{{ $Offersalary->spl_allowance}}</td>
                            <td>{{ $Offersalary->spl_allowance_td}}</td>
                            <td></td>
                        </tr>
                         <tr>
                           
                            <td>Contribution to Provident Fund</td>
                            <td>{{ $Offersalary->emp_pf}}</td>
                            <td>{{ $Offersalary->emp_pf_td}}</td>
                            <td></td>
                        </tr>
                       
                         @if($Offersalary->statutory_bonus>0)
                        <tr>
                           
                            <td>Statutory Bonus</td>
                            <td>{{ $Offersalary->statutory_bonus}}</td>
                            <td>{{ $Offersalary->statutory_bonus_year }}</td>
                            <td></td>
                        </tr>
                        @endif
                         @if($Offersalary->attendance_bonus>0)
                        <tr>
                           
                            <td>Attendance Allowance</td>
                           <td>{{ $Offersalary->attendance_bonus}}</td>
                            <td>{{ $Offersalary->attendance_bonus_year  }}</td>
                            <td></td>
                        </tr>
                       @endif
                         <tr>
                          
                             <td><b>Gross (A)</b></td>
                           <td>{{ $Offersalary->gross_td}}</td>
                            <td>{{ $Offersalary->gross_td_y}}</td>
                            <td></td>
                        </tr>
                        <tr>
                         
                             <td><b>Gratuity(B)</b></td>
                            <td></td>
                            <td>{{ $Offersalary->gratuity_td}}</td>
                            <td></td>
                        </tr>
                         @if($Offersalary->esic>0)
                         <tr>
                          
                             <td><b>Employer Contribution to ESI (C)</b></td>
                           
                            <td>{{ $Offersalary->esic}}</td>
                            <td>{{ $Offersalary->esic_y}}</td>
                             <td></td>
                        </tr>
                         @endif
                          @if($Offersalary->linked_variable_allocate>0)
                         <tr>
                          
                             <td><b>Performance Linked Variable Pay (C)</b></td>
                           <td></td>
                            <td>{{ $Offersalary->linked_variable_allocate}}</td>
                            <td>{{ $Offersalary->linked_variable_allocate_msg}}</td>
                        </tr>
                         @endif
                         <tr>
                            @if($Offersalary->linked_variable_allocate>0 || $Offersalary->esic>0)
                            <td>Cost To Company (A+B+C)</td>
                           @else
                            <td>Cost To Company (A+B)</td>
                    @endif
                            <td></td>
                            <td>{{ $Offersalary->total_cost}}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
<div class="row">
    <p><b>Notes</b></p>
</div>
<div class="row">
    <div class="col-md-12">
        <ul>
            <li>1500/1000 per month Cab deduction will be made if employee avails cab facility- applicable only for night shift employees</li>
            <li>Gross salary includes contribution of employers to Employee Provident Fund & Employee Pension Scheme</li>
            <li>Any tax liabilities arising out of the remuneration will be deducted as per the Income Tax rules</li>
            <li>Group Personal Accident Insurance coverage- as per company policy</li>
            <li>Statutory Bonus (if applicable)-This is adjustable with any Bonus, Incentive paid during the year.</li>
            <li>Medical Insurance (Not Eligible For ESIC Employee)-Rs. 2 lacs (Single), 4 lacs (Married) & 5 lacs (Married with up to 2 kids)</li>
            <li>If applicable, Rs. 2,000/- Attendance Allowance is paid based on Eli Rules as amended by Eli from time to time however there will be a Rs.1000/- deduction for every instance of absenteeism from Attendance Allowance</li>
            <li>For Chennai, Hyderabad and Indore employees, this includes Employee's contribution to Professional Tax as per slab.</li>
            <li>In addition to the above, Paid Time Off (PTO) and Holidays are available as per company's norms.<br/>
                Details of your compensation plan are confidential, please raise any queries with the undersigned only.<br/>
                “Incentives will be paid over and above the salary basis the achievement of decided sales targets.”
            </li>
 
        </ul><br/>
         Details of your compensation plan are confidential, please raise any queries with the undersigned only.

    </div>
</div>
<div class="row">
    <div class="col-md-12"><br/>
        <b>For Eli Research India Private Limited d.b.a. Enterprise Growth Accelerator a.k.a EGA</b>
    </div>
</div>
<div class="row"><br/><br/>
  <div class="col-md-12" style="width:70%; display:inline-block;">____________________________<br/><b>HR Sign Off</b><br/></div>
  
</div>
<div class="row"><br/>
  <div class="col-md-9" style="width:70%; display:inline-block;">____________________________<br/><b>Authorized Signatory</b><br/></div>
  <div class="col-md-3" style="width:30%;  display:inline-block;">_________________________<br/>{{$items['first_name']}} {{$items['last_name']}}</div>
</div>

 </div>
</div>
               
      
</body>
</html>
