
@extends('layouts.backend')

@section('content')
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css') }}"/>
  <!-- Page Content -->
      
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row"><div class="col-lg-12"><h3 class="page-header">Edit Offer #{{ $offer->id }} <a href="{{ url('/admin/offer') }}" title="Back">
                                    <button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a></h3></div>
                            
                        </div>
                    
               
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($offer, [
                            'method' => 'PATCH',
                            'url' => ['/admin/offer', $offer->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('offer.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        
        <!-- /#page-wrapper -->
     
            @section('pagespecificscripts')
    <!-- flot charts scripts-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script> 
   <script>
 


$(document).ready(function(e) {
   
   $('#dob').datepicker({
    format: 'yyyy-mm-dd'
   
});   

 $("#same_as_billing").on("change", function(){
    if (this.checked) {
      $("[name='sstreet']").val($("[name='street']").val());
      $("[name='scity']").val($("[name='city']").val());
      $("[name='sstate']").val($("[name='state']").val());
      $("[name='szip']").val($("[name='zip']").val());
      $("[name='scountry']").val($("[name='country']").val());
     
    }else{
       // alert("test");
      $("[name='sstreet']").val('');
      $("[name='scity']").val('');
      $("[name='sstate']").val('');
      $("[name='szip']").val('');
      $("[name='scountry']").val('');
    }
  });
});   



</script>
@stop
@endsection
