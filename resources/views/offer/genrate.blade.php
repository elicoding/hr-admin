
@extends('layouts.backend')

@section('content')
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css') }}"/>
<!-- Page Content -->
<style>  label.required:after {
    color: #cc0000;
    content: "*";
    font-weight: bold;
    margin-left: 5px;
}
</style>
<div class="container-fluid">
    <div class="row">
        {!! Form::model($offerdetail, [
                            'method' => 'POST',
                            'url' => ['/admin/offer/save-offer'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}
        
        <div class="col-lg-12"><br/>


            <div class="panel panel-default">
                <div class="panel-heading">
                    Offer  Candidate {{ $offer->first_name }}
                    {{ Form::hidden('user_id', $offer->id, array('id' => 'user_id')) }}
                </div>
                <div class="panel-body">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
                            {!! Form::label('company', 'Company', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('company', $company_list, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('company', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                  
                        <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
                            {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('location', $location_list, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                          <div class="form-group {{ $errors->has('function') ? 'has-error' : ''}}">
                            {!! Form::label('function:', 'Function', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('function', $function_list, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('function', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                           
                       
                              <div class="form-group {{ $errors->has('doj') ? 'has-error' : ''}}">
                            {!! Form::label('doj', 'DOJ', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                               {!! Form::text('doj', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}


                                {!! $errors->first('doj', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                        <div class="form-group {{ $errors->has('manager') ? 'has-error' : ''}}">
                            {!! Form::label('manager', 'Manager', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                               {!! Form::text('manager', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}


                                {!! $errors->first('manager', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                          <div class="form-group {{ $errors->has('cab') ? 'has-error' : ''}}">
                            {!! Form::label('cab', 'Cab ', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('cab', $cab, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('grade_level', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('designation') ? 'has-error' : ''}}">
                            {!! Form::label('business_unit', 'Business Unit', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('business_unit', $business_unit, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('business_unit', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                       
						
						 <div class="form-group {{ $errors->has('designation') ? 'has-error' : ''}}">
                            {!! Form::label('designation', 'Designation', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('designation', $designation_list, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('grade_level') ? 'has-error' : ''}}">
                            {!! Form::label('grade_level', 'Grade level', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('grade_level', $grade_level, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('grade_level', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('template') ? 'has-error' : ''}}">
                            {!! Form::label('template', 'Template', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('template', $template_list, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('template', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                           <div class="form-group {{ $errors->has('shift') ? 'has-error' : ''}}">
                            {!! Form::label('shift', 'Shift timing', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                               {!! Form::select('shift',$stiming,null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}


                                {!! $errors->first('shift', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                           <div class="form-group {{ $errors->has('calender') ? 'has-error' : ''}}">
                            {!! Form::label('calender', 'Calender', ['class' => 'col-md-4 control-label required']) !!}
                            <div class="col-md-6">
                                {!! Form::select('calender', $location_cal, null, ['class' => 'form-control'])!!}


                                {!! $errors->first('calender', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                   CTC Details
                </div>
                <div class="panel-body">
                    <div class="row">
                    <div class="col-sm-5">
                        <p>Annual CTC (INR) {!! Form::text('ctc', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'ctc'] : ['class' => 'form-control']) !!} Yearly</p>
                        <p>Monthly Salary</p>
                    </div>
                    <div class="col-sm-2"><button type="button" class="btn btn-primary btn-sm" id="genrate">Genrate</button>
                        
                        <p id="gross_sal"> (INR)</p>
                    </div>
                </div>
                   
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                            <th>Select</th>
                            <th>Salary Head</th>
                            <th>Pay Head Amount(Per Month)</th>
                            <th>Annual Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4">Earnings</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Basic</td>
                                   
                                        <td>{!! Form::text('basic', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'basic','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                    <td >{!! Form::text('basic_td', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'basic_td','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td>HRA</td>
                                    
                                       <td>{!! Form::text('hra', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'hra','readonly' => true] : ['class' => 'form-control']) !!} </td>
                                    <td>{!! Form::text('hra_td', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'hra_td','readonly' => true] : ['class' => 'form-control']) !!}</td> 
                                </tr>
                               
                                 <tr>
                                    <td></td>
                                    <td>Special Allowance</td>
                                   
                                       <td>{!! Form::text('spl_allowance', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'spl_allowance','readonly' => true] : ['class' => 'form-control']) !!} </td>
                                    <td >{!! Form::text('spl_allowance_td', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'spl_allowance_td','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                                 
                                    <tr>
                                    <td></td>
                                    <td>Employer Contribution to PF</td>
                                   
                                       <td>{!! Form::text('emp_pf', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'emp_pf','readonly' => true] : ['class' => 'form-control']) !!} </td>
                                    <td >{!! Form::text('emp_pf_td', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'emp_pf_td','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>@php
                                if(isset($offerdetail->statutory_bonus)){
                                
                                 $statutory_bonus_check = 1 ;
                                
                                }else{
                                
                               $statutory_bonus_check=null; 
                               
                               }
                               @endphp
                                   <tr>
                                    <td>{{ Form::checkbox('sbonus', 1, $statutory_bonus_check, ['class' => 'field','id'=>'sbonus']) }}</td>
                                    <td>Statutory Bonus</td>
                                   
                                       <td>{!! Form::text('statutory_bonus', null, ['class' => 'form-control annual-ctc' , 'id'=>'statutory_bonus','readonly' => true] ) !!} </td>
                                    <td >{!! Form::text('statutory_bonus_year', null,  ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'statutory_bonus_year','readonly' => true] ) !!}</td>
                                </tr>
                               @php
                                if(isset($offerdetail->attendance_bonus)){
                                
                                 $attendance_bonus_check = 1 ;
                                
                                }else{
                                
                               $attendance_bonus_check=null; 
                               
                               }
                               @endphp
                               <tr>
                                    <td>{{ Form::checkbox('abonus', 1, $attendance_bonus_check, ['class' => 'field','id'=>'abonus']) }}</td>
                                    <td>Attendance Bonus</td>
                                   
                                       <td>{!! Form::text('attendance_bonus', null, ['class' => 'form-control annual-ctc' , 'id'=>'attendance_bonus','readonly' => true] ) !!} </td>
                                    <td >{!! Form::text('attendance_bonus_year', null,  ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'attendance_bonus_year','readonly' => true] ) !!}</td>
                                </tr>
                                <tr>
                                    
                                    <td colspan="1"></td>
                                    <td>Gross Salary(A)</td>
                                    <td >{!! Form::text('gross_td', null, ('required' == 'required') ? ['class' => 'form-control  annual-ctc', 'required' => 'required' , 'id'=>'gross_td','readonly' => true] : ['class' => 'form-control']) !!} </td>
                                    <td >{!! Form::text('gross_td_y', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'gross_td_y','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                   <tr>
                                    <td></td>
                                    <td>Gratuity Benefit</td>
                                    
                                       <td>&nbsp;</td>
                                    <td >{!! Form::text('gratuity_td', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'gratuity_td','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                              <tr>
                                    <td colspan="4"></td>
                                </tr>
                                   <tr>
                                    <td></td>
                                    <td>Employer Contribution to ESI </td>
                                    
                                        <td >{!! Form::text('esic', null, ('required' == 'required') ? ['class' => 'form-control  annual-ctc', 'required' => 'required' , 'id'=>'esic','readonly' => true] : ['class' => 'form-control']) !!} </td>
                                        <td>{!! Form::text('esic_y', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'esic_y','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                                <tr>
                                    
                                    <td colspan="2"></td>
                                    <td>Employer Payment for Health Insurance Premium</td>
                                    
                                    <td>As per policy</td>
                                </tr>
                                
                                <tr>
                                    
                                    <td colspan="2"></td>
                                    <td>Employer  Payment for GPA** cover</td>
                                   
                                    <td>As per policy</td>
                                </tr>
                               
                                <tr>
                                    
                                    <td colspan="2"></td>
                                    <td>Non-Cash Benefits (B)</td>
                                    
                                    <td id="gratuity_td1">0</td>
                                </tr>
                                 <tr>
                                    
                                    <td >{{ Form::checkbox('ppv', 1, null, ['class' => 'field','id'=>'ppv']) }}</td>
                                    <td ></td>
                                    <td>Performance Linked Variable Pay*** (C)</td>
                                   
                                    <td id="linked_variable">{!! Form::text('linked_variable_allocate', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'linked_variable_allocate', 'disabled' => 'disabled',] : ['class' => 'form-control']) !!}<br/><br/>
                                        {!! Form::text('linked_variable_allocate_msg', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'linked_variable_allocate_msg', 'disabled' => 'disabled',] : ['class' => 'form-control']) !!}
                                    </td>
                                </tr>
                                 <tr>
                                    
                                    <td colspan="2"></td>
                                    <td id="total_cost_td">Cost To Company (A+B)</td>
                                    
                                    <td >{!! Form::text('total_cost', null, ('required' == 'required') ? ['class' => 'form-control annual-ctc', 'required' => 'required' , 'id'=>'total_cost','readonly' => true] : ['class' => 'form-control']) !!}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                     </div>
            </div>
            <div class="form-group">
    <div class="col-md-offset-4 col-md-4" id="gen-button">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-default']) !!}
        <button type="reset" class="btn btn-default">Reset Button</button>
    </div>
</div>
        </div>
        <!-- /.col-lg-12 -->
          {!! Form::close() !!}
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->


<!-- flot charts scripts-->
  @section('pagespecificscripts')
    <!-- flot charts scripts-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script> 
   <script>
 


$(document).ready(function(e) {
    $("#sbonus").prop("disabled", true);
    $("#abonus").prop("disabled", true);
   var linked_variable_allocate=$('#linked_variable_allocate').val();
   if(linked_variable_allocate>0){
     $("#ppv").prop("checked", true);  
      $('#linked_variable_allocate').prop("disabled", false); 
     $('#linked_variable_allocate_msg').prop("disabled", false);
   }
   $('#doj').datepicker({
    format: 'yyyy-mm-dd',
   
}); 
$("#ppv").click(function(){
    var total_cost_amt='';
     var ctc=$('#ctc').val();
     var basic=$('#basic').val();
     var linked_variable_allocate=$('#linked_variable_allocate').val();
 if($(this).is(":checked")) {
  // alert($(this).val());   
  $('#linked_variable_allocate').prop("disabled", false);
  $('#linked_variable_allocate_msg').prop("disabled", false);
   }else{
       $('#linked_variable_allocate').val('');
    $('#linked_variable_allocate').prop("disabled", true); 
     $('#linked_variable_allocate_msg').prop("disabled", true);
     gratuity_amt=Math.round((basic/26)*15);
      total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt);
        $('#total_cost').val(total_cost_amt);
        $('#total_cost_td').html('Cost To Company (A+B)'); 
   }
});
$("#sbonus").click(function(){
    var total_cost_amt='';
     var spl=$('#spl_allowance').val();
     var spl_y=$('#spl_allowance_td').val();
     
     var basic=$('#basic').val();
     var linked_variable_allocate=$('#linked_variable_allocate').val();
     if(basic<=21000){
    if($(this).is(":checked")) {
        $('#statutory_bonus').val('2000');
        $('#statutory_bonus_year').val('24000');
        var spl1=parseInt(spl)-parseInt(2000);
        $('#spl_allowance').val(spl1);
        $('#spl_allowance_td').val(spl1*12);
         // statutory_bonus
      }else{
         var sp1=$('#spl_allowance').val();
        // alert(sp1);
        // alert(spl);
          var spl2=parseInt(sp1)+2000;
          // alert(spl2)
          $('#spl_allowance').val(spl2);
          $('#spl_allowance_td').val(spl2*12);
          $('#statutory_bonus').val('');
          $('#statutory_bonus_year').val('');
      }
  }else{
    alert("Basic Salary should be less than equal to 21000");  
  }
});
$("#abonus").click(function(){
    var total_cost_amt='';
     var spl=$('#spl_allowance').val();
     var spl_y=$('#spl_allowance_td').val();
     
     var basic=$('#basic').val();
     var linked_variable_allocate=$('#linked_variable_allocate').val();
    if($(this).is(":checked")) {
        $('#attendance_bonus').val('2000');
        $('#attendance_bonus_year').val('24000');
        var spl1=parseInt(spl)-parseInt(2000);
        $('#spl_allowance').val(spl1);
        $('#spl_allowance_td').val(spl1*12);
         // statutory_bonus
      }else{
         var sp1=$('#spl_allowance').val();
        // alert(sp1);
        // alert(spl);
          var spl2=parseInt(sp1)+2000;
          // alert(spl2)
          $('#spl_allowance').val(spl2);
          $('#spl_allowance_td').val(spl2*12);
          $('#attendance_bonus').val('');
        $('#attendance_bonus_year').val('');
      }
});
$("#template").bind("change", function(e) {
    var currentSel= $( "#template" ).val();
    
});
$('#genrate').on('click', function () {
   var currentSel= $( "#template" ).val();
    var locationSel=$( "#location" ).val();
     var ctc=$('#ctc').val();
     if(ctc>100000){
    //alert(locationSel);
   $('#gross_td').val('');
   $('#gross_td_y').val('');
   $('#basic').val('');
   $('#basic_td').val('');
   
   $('#hra').val('');
   $('#hra_td').val('');
   $('#gratuity_td').val('');
   $('#total_cost').val('');
   $('#emp_pf').val('');
   $('#emp_pf_td').val('');
   $('#statutory_bonus').val('');
   $('#statutory_bonus_year').val('');
   $('#spl_allowance').val('');
   $('#spl_allowance_td').val('');
   $('#attendance_bonus').val('');
   $('#attendance_bonus_year').val('');
   $('#linked_variable_allocate').val('');
   $('#linked_variable_allocate_msg').val('');

   
   
     var gross=ctc/12;
     var basic;
     var hra;
     var c_allowance=1600;
     var y_c_allowance=1600*12;
     var emp_pf;
     var y_emp_pf;
     var y_basic_td;
     var y_hra_td;
     var spl_allowance;
     var y_spl_allowance;
     var gratuity_amt;
     var total_cost_amt;
     if(locationSel=='Noida'){ 
     gross=Math.round(gross); 
      if(gross<23500){
      basic=9382;   
     }else{
       basic=Math.round(gross*.4);  
     }
    }else if(locationSel=='Indore'){
      gross=Math.round(gross); 
      if(gross<23900){
      basic=9560;   
     }else{
       basic=Math.round(gross*.4);  
     }
    }else if(locationSel=='Hyderabad'){
      gross=Math.round(gross); 
      if(gross<24000){
      basic=9589;   
     }else{
       basic=Math.round(gross*.4);  
     }
    }else if(locationSel=='Faridabad'){
      gross=Math.round(gross); 
      if(gross<24720){
        basic=9888;   
       }else{
         basic=Math.round(gross*.4);  
       }
    }else{
     gross=Math.round(gross); 
      if(gross<21650){
      basic=6658;   
     }else{
       basic=Math.round(gross*.4);  
     }   
    }
     y_basic_td=basic*12;
     if(basic<15000 ){
        emp_pf=Math.round(basic*.12);
     }else{
         emp_pf=1800;
     }
     y_emp_pf=emp_pf*12;
     gratuity_amt=Math.round((basic/26)*15);
     ctc=gross*12;
   if(currentSel=='Offer letterELI')
   {
     $('#sbonus').prop('checked', false); 
     $('#abonus').prop('checked', false); 
     hra=Math.round(basic*.5);
    
   /// alert(gross);
     
    y_hra_td=hra*12;
    var tot=basic+hra;
    spl_allowance=Math.round(gross-tot-emp_pf);
    if(spl_allowance>0){
         var y_spl_allowance=spl_allowance*12;
    }else{
         var y_spl_allowance=0;
    }
   
   
    total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt);
     $('#gross_sal').html(gross+ "  (INR)");
     $('#gross_td').val(gross); 
     $('#gross_td_y').val(ctc);
     $('#basic').val(basic);
     $('#hra').val(hra);
     $('#basic_td').val(y_basic_td);
     $('#hra_td').val(y_hra_td);
    
     $('#emp_pf').val(emp_pf);
     $('#emp_pf_td').val(y_emp_pf);
     $('#spl_allowance').val(spl_allowance);
     $('#spl_allowance_td').val(y_spl_allowance);
     $('#gratuity_td').val(gratuity_amt);
     $('#gratuity_td1').html(gratuity_amt);
     $('#total_cost').val(total_cost_amt);
     $('#esic').val(0);
      $('#esic_y').val(0);
    
      // alert(currentSel);  
       $("#sbonus").prop("disabled", false);  
       $("#abonus").prop("disabled", false);  
         
     }else{
        // alert(currentSel);
     
      
      $("#sbonus").prop("disabled", true);
      $("#abonus").prop("disabled", true);
   
     var tot=basic+emp_pf+2000;
     var remain_balance=gross-tot;
     var hra=Math.round(basic/2);
     if(hra>remain_balance){
         hra=Math.round(remain_balance);
     }else{
        var spl_all= remain_balance-hra;
        
     }
        if(spl_all>0){
         var y_spl_allowance=spl_all*12;
    }else{
         var y_spl_allowance=0;
    }
      y_hra_td=hra*12;
      total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt);
      var esic=Math.round((gross-emp_pf)*4.75/100);
     total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt)+esic*12;
     $('#gross_sal').html(gross+ "  (INR)");
     $('#gross_td').val(gross); 
     $('#gross_td_y').val(ctc);
     $('#basic').val(basic);
     $('#basic_td').val(y_basic_td);
     $('#hra').val(hra);
     
     $('#hra_td').val(y_hra_td);
     
     $('#emp_pf').val(emp_pf);
     $('#emp_pf_td').val(y_emp_pf);
     $('#statutory_bonus').val('2000');
     $('#statutory_bonus_year').val('24000');
     $('#spl_allowance').val(spl_all);
     $('#spl_allowance_td').val(y_spl_allowance);
     $('#abonus').prop('checked', false);
     $('#gratuity_td').val(gratuity_amt);
     $('#gratuity_td1').html(gratuity_amt);
     
    
      $('#esic').val(esic);
      $('#esic_y').val(esic*12);
       $('#total_cost').val(total_cost_amt);
    //alert(esic);
     }
      $('#gen-button').show();
    
 }else{
    $('#gen-button').hide();
    alert("Please Enter Valid CTC");
 }
       // alert(gross);
});
$('#linked_variable_allocate').on('change', function () {
    var total_cost_amt='';
     var ctc=$('#ctc').val();
     var basic=$('#basic').val();
     var linked_variable_allocate=$('#linked_variable_allocate').val();
     if(linked_variable_allocate>0){
      gratuity_amt=Math.round((basic/26)*15);
      total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt)+ parseInt(linked_variable_allocate);
        $('#total_cost').val(total_cost_amt);
        $('#total_cost_td').html('Cost To Company (A+B+C)');
    }else{
       gratuity_amt=Math.round((basic/26)*15);
      total_cost_amt=parseInt(ctc) + parseInt(gratuity_amt);
        $('#total_cost').val(total_cost_amt);
        $('#total_cost_td').html('Cost To Company (A+B)'); 
    }
    });

});



</script>
@stop
@endsection
