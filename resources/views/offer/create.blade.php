@extends('layouts.backend')

@section('content')
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css') }}"/>
  <!-- Page Content -->
      
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12"><br/>

                           @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/offer', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('offer.form')

                        {!! Form::close() !!}
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        
        <!-- /#page-wrapper -->
     
            @section('pagespecificscripts')
    <!-- flot charts scripts-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script> 
   <script>
 


$(document).ready(function(e) {
   
   $('#dob').datepicker({
    format: 'yyyy-mm-dd',
   
});   

 $("#same_as_billing").on("change", function(){
    if (this.checked) {
      $("[name='sstreet']").val($("[name='street']").val());
      $("[name='scity']").val($("[name='city']").val());
      $("[name='sstate']").val($("[name='state']").val());
      $("[name='szip']").val($("[name='zip']").val());
      $("[name='scountry']").val($("[name='country']").val());
     
    }else{
       // alert("test");
      $("[name='sstreet']").val('');
      $("[name='scity']").val('');
      $("[name='sstate']").val('');
      $("[name='szip']").val('');
      $("[name='scountry']").val('');
    }
  });
});   



</script>
@stop
@endsection
