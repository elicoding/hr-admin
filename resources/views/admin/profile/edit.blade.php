@extends('layouts.backend')

@section('content')

<br/>
   
          
            <div class="col-md-12">

             
                    <div class="panel panel-default">
                        
                 <div class="panel-heading">Edit Profile</div>
                 <div class="panel-body">
                                       @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($user, [
                            'method' => 'PATCH',
                            'url' => ['/admin/profile', $user->id],
                            'class' => 'form-horizontal'
                        ]) !!}

                        @include ('admin.profile.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
                 </div>
      

                     

                    </div>
          
            </div>


@endsection
