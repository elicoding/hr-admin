@if (Auth::user()->roles->first()->name=='admin')
<ul class="nav" id="side-menu">
                 
                        <li>
                            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> User Managment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/users">List User</a>
                                </li>
                                <li>
                                    <a href="/admin/roles">List Roles</a>
                                </li>
                                 <li>
                                    <a href="/admin/offer">List permissions</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                     
                      <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Offer Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/offer">List Offer</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> Report Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/report">Offer Report</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>


@endif
@if (Auth::user()->roles->first()->name=='recuiter')

                    <ul class="nav" id="side-menu">
                 
                        <li>
                            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Offer Managment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/offer">View Offer</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                     
                      
                        
                    </ul>
               
            <!-- /.navbar-static-side -->
        
@endif