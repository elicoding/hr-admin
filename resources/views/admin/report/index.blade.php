@extends('layouts.backend')

@section('content')
  
    
<div class="col-md-12"><h3 class="page-header">Report <a href="{{ url('admin/export-file/xls') }}" class="btn btn-success btn-sm pull-right" title="Download Xls report">
                            <i class="fa fa-download" aria-hidden="true"></i> Download Xls
                        </a></h3></div> 
<div class="col-md-12">
<div class="panel panel-default">
    <div class="panel-heading">Admin  Report</div>
    <div class="panel-body">
         <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Offer ID</th><th>Name of Candidate</th>
                                        <th>DOB</th>
                                        
                                        <th>	Manager Name</th>
                                        <th>	BU</th>
                                        <th>	Designation</th>
                                        <th>	Function</th>
                                        <th>Personal Email ID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                      <td>{{ $item->offer_number }}</td>   
                                       <td>{{ $item->first_name." ".$item->last_name }}</td>  
                                        <td>{{ $item->dob }}</td>  
                                       <td>{{ $item->manager }}</td> 
                                       <td>{{ $item->business_unit }}</td> 
                                       <td>{{ $item->designation }}</td> 
                                       <td>{{ $item->function }}</td> 
                                       <td>{{ $item->email }}</td> 
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
    </div>
</div>
    </div>

     
      
@section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{ asset('css/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<script type="text/javascript">
 $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
 </script>
@stop
@endsection
