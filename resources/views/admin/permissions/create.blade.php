@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
                    <h3 class="page-header"><a href="{{ url('/admin/permissions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a></h3>
                </div>
     <div class="col-md-12">

             
                    <div class="panel panel-default">
                        
                 <div class="panel-heading">Create Permission</div>
                 <div class="panel-body">
      @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/permissions', 'class' => 'form-horizontal']) !!}

                        @include ('admin.permissions.form')

                        {!! Form::close() !!}
                     
                 </div>
                    </div>
     </div>



@endsection