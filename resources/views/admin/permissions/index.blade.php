@extends('layouts.backend')

@section('content')
 <div class="col-lg-12">
                    <h3 class="page-header">Permissions Managment <a href="{{ url('/admin/permissions/create') }}" class="btn btn-success btn-sm pull-right" title="Add New Offer">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a></h3>
                </div>


   <div class="col-lg-12">
   
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                           Permissions Listing
                           
                          
                           
                        
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Name</th><th>Label</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td><a href="{{ url('/admin/permissions', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->label }}</td>
                                        <td>
                                            
                                            <a href="{{ url('/admin/permissions/' . $item->id . '/edit') }}" title="Edit Permission"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/permissions', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Permission',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
   </div>



   @section('pagespecificscripts')
<!-- flot charts scripts-->
<script src="{{ asset('css/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('css/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<script type="text/javascript">
 $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
 </script>
@stop
@endsection
