<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   

    <!-- Styles -->
   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hr Admin</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('css/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/vendor/morrisjs/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dist/css/sb-admin-2.css') }}" rel="stylesheet">
     <link href="{{ asset('css/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
   

    <!-- Custom Fonts -->
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header" >
               
                
                <a href="{{ url('/admin') }}" class="navbar-brand" > 
                    
                    
                    <img src="{{ asset('../img/ega-logo.png') }}"/><span class="sr-only">(current)</span></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            
                 @guest
                            <li><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
                            
                        @else
                          <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url('admin/profile') }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                      
                        <li class="divider"></li>
                        <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                       
                        @endguest
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                 @include('admin.sidebar')
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
          
            <!-- /.row -->
            <div class="row"> 
                @if (Session::has('flash_message'))
                <br/>
                <div class="container">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_message') }}
                    </div>
                </div>
            @endif

            @yield('content')
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    
        <script src="{{ asset('css/vendor/jquery/jquery.min.js') }}"></script>
         <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('css/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('css/vendor/metisMenu/metisMenu.min.js') }}"></script>
      
        <script src="{{ asset('css/dist/js/sb-admin-2.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            // Navigation active
            $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
        });
    </script>

    @yield('pagespecificscripts');
   
</body>

</html>
